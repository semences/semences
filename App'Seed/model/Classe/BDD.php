<?php

namespace Classe;

/**
 * Représente la base de donnée c'est à dire l'accès à la base de
 * données pour l'application seed basé sur MySQL
 */
class BDD
{
    protected $pdo;

    public function __construct($host, $database, $user, $password)
    {
        try {
            $this->pdo = new \PDO(
                'mysql:dbname='.$database.';host='.$host,
                $user,
                $password
            );
            $this->pdo->exec('SET CHARSET UTF8');
        } catch (\PDOException $exception) {
            die('Impossible de se connecter au serveur MySQL');
        }
    }

    /**
     * Récupère un résultat exactement
     */
    protected function fetchOne(\PDOStatement $query)
    {
        if ($query->rowCount() != 1) {
            return false;
        } else {
            return $query->fetch();
        }
    }

    public function getAllLegume() {
        $sql = "SELECT idLegume,nom FROM legume ORDER BY nom";
        $query = $this->pdo->prepare($sql);
        $query->execute();
        return $query->fetchAll();
    }
	
	
	//get, set, delete, update legume
	public function getLegume($idLeg) {
		$req = $this->pdo->prepare('SELECT * FROM legume WHERE idLegume=?');
		$req->execute(array($idLeg));
		if ($req->rowCount())
			return $req->fetch();
	}
	
	public function addLegume($nom_legume) {
		$req = $this->pdo->prepare('SELECT COUNT(*) as nb FROM legume WHERE nom=?');
		$req->execute(array($nom_legume));
		$req = $req->fetch();
		if ($req['nb'] == 0) {
			$query = $this->pdo->prepare('INSERT INTO legume (nom) VALUES (?)');
			$query->execute(array($nom_legume));
			return 'success';
		}else{
			return 'exist';
		}
	}
	public function updateLegume($idLegume,$nomLegume){
		if(!empty($idLegume)){
			$query = $this->pdo->prepare('UPDATE legume 
										SET nom=? 
										WHERE idLegume=?');
			$query->execute(array($nomLegume, $idLegume));
			return "success";
		}else{
			return "noChoice";
		}
	}
	
	public function deleteLegume($idLegume){
		if(!is_null($idLegume)){
			$query = $this->pdo->prepare('DELETE FROM legume WHERE idLegume=?');
			$query->execute(array($idLegume));
			return "success";
		}
	}
	
	
	
	//get et set semencier
	public function getSemencier($idSemencier) {
		$req = $this->pdo->prepare('SELECT * FROM Semencier WHERE idSemencier=?');
		$req->execute(array($idSemencier));
		if ($req->rowCount())
			return $req->fetch();
	}
	
	public function setSemencier($nom_semencier) {
		$req = $this->pdo->prepare('SELECT COUNT(*) as nb FROM Semencier WHERE nom=?');
		$req->execute(array($nom_semencier));
		$req = $req->fetch();
		if ($req['nb'] == 0) {
			$query = $this->pdo->prepare('INSERT INTO Semencier (nom) VALUES (?)');
			$query->execute(array($nom_semencier));
			return 'success';
		}else{
			return 'exist';
		}
	}

	public function getAllSemenciers() {
		$sql = "SELECT * FROM Semencier";
		$query = $this->pdo->prepare($sql);
		$query->execute();
		return $query->fetchAll();
	}
	
	public function updateSemencier($idSemencier,$nom_semencier) {
		if(!empty($idSemencier)){
			$req = $this->pdo->prepare('UPDATE Semencier 
										SET nom=?
										WHERE idSemencier =?');
			$req->execute(array($nom_semencier, $idSemencier));
			return "success";
		}
	}
	
	public function deleteSemencier($idSemencier) {
		if(!empty($idSemencier)){
			$req = $this->pdo->prepare('DELETE FROM Semencier 
										WHERE idSemencier =?');
			$req->execute(array($idSemencier));
			return "success";
		}
	}

	
	//get et set variété 
	public function getVariete($idVar) {
		$sql = "SELECT * FROM Variete WHERE Variete.idVariete=?";
		$req = $this->pdo->prepare($sql);
		$req->execute(array($idVar));
		if ($req->rowCount())
			return $req->fetch();
	}
	
	public function setVariete($nom_variete, $code_leg, $commentaire, $idLeg, $idSemencier) {
		$req = $this->pdo->prepare('SELECT COUNT(*) as nb FROM Variete WHERE nom=? AND idLegume=?');
		$req->execute(array($nom_variete,$idLeg));
		$req = $req->fetch();
		if ($req['nb'] == 0) {
			$query = $this->pdo->prepare('INSERT INTO variete (nom, codeLegume, commentaire, idLegume, idSemencier) VALUES (?,?,?,?,?)');
			$query->execute(array($nom_variete, $code_leg, $commentaire, $idLeg, $idSemencier));
			return 'success';
		}else{
			return 'exist';
		}
	}

	public function updateVariete($nom_variete, $code_leg, $commentaire, $idLeg, $idSemencier,$idVar) {
		$sql = "UPDATE variete SET nom=?, codeLegume=?, commentaire=?, idLegume=?, idSemencier=? WHERE idVariete=?";
		$query = $this->pdo->prepare($sql);
		return $query->execute(array($nom_variete, $code_leg, $commentaire, $idLeg, $idSemencier, $idVar));
	}

	public function deleteVariete($id) {
		$sql = "DELETE FROM Variete WHERE idVariete = :id";
		$query = $this->pdo->prepare($sql);
		$query->bindParam(":id",$id);
		return $query->execute();		
	}
	
	//get et set emplacement 
	public function getEmplacement($idEmp) {
		$req = $this->pdo->prepare('SELECT * FROM emplacement WHERE idEmplacement=?');
		$req->execute(array($idEmp));
		if ($req->rowCount())
			return $req->fetch();
	}
	
	public function setEmplacement($parcelle, $carre_culture, $planche, $codeEmplacement, $commentaire) {
		$query = $this->pdo->prepare('INSERT INTO Emplacement (parcelle, carreCulture, planche, codeEmplacement, commentaire) VALUES (?,?,?,?,?)');
		return $query->execute(array($parcelle, $carre_culture, $planche, $codeEmplacement, $commentaire));
	}

	public function updateEmplacement($parcelle, $carre_culture, $planche, $codeEmplacement, $commentaire, $idEmplacement){
		$sql = "UPDATE Emplacement SET parcelle=?, carreCulture=?, planche=?, codeEmplacement=?, commentaire=? WHERE idEmplacement=?";
		$query = $this->pdo->prepare($sql);
		return $query->execute(array($parcelle, $carre_culture, $planche, $codeEmplacement, $commentaire, $idEmplacement));
	}

	public function getAllEmplacement() {
		$sql = "SELECT * FROM Emplacement;";
		$query = $this->pdo->prepare($sql);
		$query->execute();
		return $query->fetchAll();
	}

	public function deleteEmplacement($id) {
		$sql = "DELETE FROM Emplacement WHERE idEmplacement = :id";
		$query = $this->pdo->prepare($sql);
		$query->bindParam(":id",$id);
		return $query->execute();		
	}
	
	//get et set emplacement_reserve
	public function getEmplacementReserve($idEmp_reserve) {
		$req = $this->pdo->prepare('SELECT EmplacementReserve.idEmplacementReserve,EmplacementReserve.idEmplacement,EmplacementReserve.idVariete,Emplacement.codeEmplacement,Variete.nom,Legume.nom as legume FROM EmplacementReserve,Emplacement,Variete,Legume WHERE idEmplacementReserve=? AND EmplacementReserve.idVariete = Variete.idVariete AND EmplacementReserve.idEmplacement = Emplacement.idEmplacement AND Variete.idLegume = Legume.idLegume');
		$req->execute(array($idEmp_reserve));
		if ($req->rowCount())
			return $req->fetch();
	}
	
	public function getAllEmplacementReserve() {
		$sql = "SELECT EmplacementReserve.idEmplacementReserve,Emplacement.codeEmplacement, Legume.nom as nomLegume, Variete.nom as nomVariete FROM EmplacementReserve, Emplacement, Legume, Variete WHERE EmplacementReserve.idVariete = Variete.idVariete AND Legume.idLegume = Variete.idLegume AND EmplacementReserve.idEmplacement = Emplacement.idEmplacement ORDER BY EmplacementReserve.idEmplacementReserve DESC";
		$query = $this->pdo->prepare($sql);
		$query->execute();
		return $query->fetchAll();
	}

	public function setEmplacementReserve($idEmp,$idVar) {
		$req = $this->pdo->prepare('SELECT COUNT(*) as nb FROM EmplacementReserve WHERE idEmplacement=? AND idVariete=?');
		$req->execute(array($idEmp,$idVar));
		$req = $req->fetch();
		if ($req['nb'] == 0) {
			$query = $this->pdo->prepare('INSERT INTO EmplacementReserve (idEmplacement, idVariete) VALUES (?,?)');
			$query->execute(array($idEmp,$idVar));
			return 'success';
		}else{
			return 'exist';
		}
	}

	public function deleteEmplacementReserve($id) {
		$sql = "DELETE FROM EmplacementReserve WHERE idEmplacementReserve = :id";
		$query = $this->pdo->prepare($sql);
		$query->bindParam(":id",$id);
		return $query->execute();	
	}
	
	//get et set planning previsionnel 
	public function getPrevisionnel($idPrev) {
		$req = $this->pdo->prepare('SELECT * FROM previsionnel WHERE idPrevisionnel=?');
		$req->execute(array($idPrev));
		$result = $req->fetch();
		$result['semis'] = date("d/m/Y", strtotime($result['semis']));
		$result['plantation'] = date("d/m/Y", strtotime($result['plantation']));
		$result['debutRecolte'] = date("d/m/Y", strtotime($result['debutRecolte']));
		$result['finRecolte'] = date("d/m/Y", strtotime($result['finRecolte']));
		return $result;
	}

	public function addPrevisionnel($prep_sol, $lutte_adv, $semis, $plantation, $debutRecolte, $finRecolte, $idEmplacementReserve) {
		$query = $this->pdo->prepare('INSERT INTO Previsionnel (preparationSol,lutteAdventices,semis,plantation,debutRecolte,finRecolte,idEmplacementReserve) VALUES (?,?,?,?,?,?,?)');
		return $query->execute(array($prep_sol, $lutte_adv, $semis, $plantation, $debutRecolte, $finRecolte, $idEmplacementReserve));
	}

	public function setPrevisionnel($prep_sol, $lutte_adv, $semis, $plantation, $debutRecolte, $finRecolte,$idPrevisionnel) {
		$query = $this->pdo->prepare('UPDATE Previsionnel SET preparationSol=:preparationSol,lutteAdventices=:lutteAdventices,semis=:semis,plantation=:plantation,debutRecolte=:debutRecolte,finRecolte=:finRecolte WHERE idPrevisionnel=:idPrevisionnel');
		$query->bindParam(":preparationSol", $prep_sol);
		$query->bindParam(":lutteAdventices", $lutte_adv);
		$query->bindParam(":semis",$semis);
		$query->bindParam(":plantation",$plantation);
		$query->bindParam(":debutRecolte",$debutRecolte);
		$query->bindParam(":finRecolte",$finRecolte);
		$query->bindParam(":idPrevisionnel",$idPrevisionnel);
		return $query->execute();
	}

	public function getAllPrevisionnel($idEmplacementReserve) {
		$sql = "SELECT * FROM Previsionnel WHERE idEmplacementReserve=:id";
		$query = $this->pdo->prepare($sql);
		$query->bindParam(":id",$idEmplacementReserve);
		$query->execute();
		$resultats = $query->fetchAll();
		$retour = array();
		foreach ($resultats as $key => $prevision) {
			$retour[$key]['idPrevisionnel'] = $prevision['idPrevisionnel'];
			$retour[$key]['preparationSol'] = $prevision['preparationSol'];
			$retour[$key]['lutteAdventices'] = $prevision['lutteAdventices'];
			$retour[$key]['semis'] = date("d/m/Y", strtotime($prevision['semis']));
			$retour[$key]['plantation'] = date("d/m/Y", strtotime($prevision['plantation']));
			$retour[$key]['debutRecolte'] = date("d/m/Y", strtotime($prevision['debutRecolte']));
			$retour[$key]['finRecolte'] = date("d/m/Y", strtotime($prevision['finRecolte']));
		}
		return $retour;
	}

	public function getAllPrevisions() {
		$sql = "SELECT * FROM Previsionnel";
		$query = $this->pdo->prepare($sql);
		$query->execute();
		return $query->fetchAll();
	}

	public function deletePrevisionnel($id) {
		$sql = "DELETE FROM Previsionnel WHERE idPrevisionnel = :id";
		$query = $this->pdo->prepare($sql);
		$query->bindParam(":id",$id);
		return $query->execute();		
	}
	
	public function getAllPrevisionnelAndVariete() {
		$sql = "SELECT Variete.codeLegume, Emplacement.codeEmplacement, Previsionnel.idPrevisionnel,Previsionnel.semis,Previsionnel.plantation, Previsionnel.debutRecolte, Previsionnel.finRecolte FROM emplacementreserve , Previsionnel, Variete, Emplacement 
				WHERE Previsionnel.idEmplacementReserve = emplacementreserve.idEmplacementReserve AND emplacementreserve.idEmplacement = emplacement.idEmplacement AND emplacementreserve.idVariete = Variete.idVariete 
				ORDER BY Variete.codeLegume";
		$query = $this->pdo->prepare($sql);
		$query->execute();
		$resultats = $query->fetchAll();
		$retour = array();
		foreach ($resultats as $key => $previsionVariete) {
			$retour[$key]['codeLegume'] = $previsionVariete['codeLegume'];
			$retour[$key]['codeEmplacement'] = $previsionVariete['codeEmplacement'];
			$retour[$key]['idPrevisionnel'] = $previsionVariete['idPrevisionnel'];
			$retour[$key]['semis'] = date("d/m/Y", strtotime($previsionVariete['semis']));
			$retour[$key]['plantation'] = date("d/m/Y", strtotime($previsionVariete['plantation']));
			$retour[$key]['debutRecolte'] = date("d/m/Y", strtotime($previsionVariete['debutRecolte']));
			$retour[$key]['finRecolte'] = date("d/m/Y", strtotime($previsionVariete['finRecolte']));
		}
		return $retour;
	}
	
	//get et set suivi
	public function getPrevisionnelSuivi($idPrevisionnel, $idEmplacementReserve) {
		$req = $this->pdo->prepare('SELECT * FROM Suivi WHERE idPrevisionnel=? AND idEmplacementReserve=?');
		$req->execute(array($idPrevisionnel,$idEmplacementReserve));
		$result = array();
		if ($req->rowCount()){
			$result = $req->fetch();
			if($result['preparationSol'] != null)
				$result['preparationSol'] = date("d/m/Y", strtotime($result['preparationSol']));
			
			if($result['lutteAdventices'] != null)
				$result['lutteAdventices'] = date("d/m/Y", strtotime($result['lutteAdventices']));
			
			if($result['semis'] != null)
				$result['semis'] = date("d/m/Y", strtotime($result['semis']));

			if($result['plantation'] != null)
				$result['plantation'] = date("d/m/Y", strtotime($result['plantation']));

			if($result['debutRecolte'] != null)
				$result['debutRecolte'] = date("d/m/Y", strtotime($result['debutRecolte']));

			if($result['finRecolte'])
				$result['finRecolte'] = date("d/m/Y", strtotime($result['finRecolte']));
		}
		return $result;
	}

	public function getSuivi($idSuivi) {
		$req = $this->pdo->prepare('SELECT * FROM Suivi WHERE idSuivi=?');
		$req->execute(array($idSuivi));
		$result = array();
		if ($req->rowCount()){
			$result = $req->fetch();
			if($result['preparationSol'] != null)
				$result['preparationSol'] = date("d/m/Y", strtotime($result['preparationSol']));
			
			if($result['lutteAdventices'] != null)
				$result['lutteAdventices'] = date("d/m/Y", strtotime($result['lutteAdventices']));
			
			if($result['semis'] != null)
				$result['semis'] = date("d/m/Y", strtotime($result['semis']));

			if($result['plantation'] != null)
				$result['plantation'] = date("d/m/Y", strtotime($result['plantation']));

			if($result['debutRecolte'] != null)
				$result['debutRecolte'] = date("d/m/Y", strtotime($result['debutRecolte']));

			if($result['finRecolte'])
				$result['finRecolte'] = date("d/m/Y", strtotime($result['finRecolte']));
		}
		return $result;
	}

	public function getSuiviByPrevisionnel($idPrevisionnel) {
		$req = $this->pdo->prepare('SELECT * FROM Suivi WHERE idPrevisionnel=?');
		$req->execute(array($idPrevisionnel));
		$result = array();
		if ($req->rowCount()){
			$result = $req->fetch();
			if($result['preparationSol'] != null)
				$result['preparationSol'] = date("d/m/Y", strtotime($result['preparationSol']));
			
			if($result['lutteAdventices'] != null)
				$result['lutteAdventices'] = date("d/m/Y", strtotime($result['lutteAdventices']));
			
			if($result['semis'] != null)
				$result['semis'] = date("d/m/Y", strtotime($result['semis']));

			if($result['plantation'] != null)
				$result['plantation'] = date("d/m/Y", strtotime($result['plantation']));

			if($result['debutRecolte'] != null)
				$result['debutRecolte'] = date("d/m/Y", strtotime($result['debutRecolte']));

			if($result['finRecolte'])
				$result['finRecolte'] = date("d/m/Y", strtotime($result['finRecolte']));

			return $result;
		}
		else return null;
		
	}

	public function getAllSuivis() {
		$req = $this->pdo->query('SELECT * FROM suivi');
	    $data = $req->fetchAll();
	    return $data;
	}

	public function setSuivi($prep_sol, $lutte_adv, $semis, $plantation, $debutRecolte, $finRecolte, $quantite, $unite,$qualite,$commentaire, $idPrevisionnel, $idEmplacementReserve) {
		$req = $this->pdo->prepare('SELECT COUNT(*) as nb FROM Suivi WHERE idPrevisionnel=?');
		$req->execute(array($idPrevisionnel));
		$req = $req->fetch();
		if ($req['nb'] == 0) {
			$query = $this->pdo->prepare('INSERT INTO Suivi (preparationSol,lutteAdventices,semis,plantation,debutRecolte,finRecolte,quantite, unite, qualite, commentaire, idPrevisionnel, idEmplacementReserve) VALUES (:preparationSol,:lutteAdventices,:semis,:plantation,:debutRecolte,:finRecolte,:quantite,:unite,:qualite,:commentaire,:idPrevisionnel,:idEmplacementReserve)');
			if($prep_sol == null) {
				$query->bindValue(":preparationSol", null, \PDO::PARAM_NULL);
			} else {
				$query->bindParam(":preparationSol",$prep_sol);
			}

			if(is_null($lutte_adv)) {
				$query->bindValue(":lutteAdventices", null, \PDO::PARAM_NULL);
			} else {
				$query->bindParam(":lutteAdventices",$lutte_adv);
			}

			if($semis == null) {
				$query->bindValue(":semis", null, \PDO::PARAM_NULL);
			} else {
				$query->bindParam(":semis", $semis);
			}

			if($plantation == null) {
				$query->bindValue(":plantation", null, \PDO::PARAM_NULL);
			} else {
				$query->bindParam(":plantation", $plantation);
			}

			if($debutRecolte == null) {
				$query->bindValue(":debutRecolte", null, \PDO::PARAM_NULL);
			} else {
				$query->bindParam(":debutRecolte", $debutRecolte);
			}

			if($finRecolte == null) {
				$query->bindValue(":finRecolte", null, \PDO::PARAM_NULL);
			} else {
				$query->bindParam(":finRecolte", $finRecolte);
			}

			if($quantite == null) {
				$query->bindValue(":quantite", null, \PDO::PARAM_NULL);
			} else {
				$query->bindParam(":quantite", $quantite);
			}

			if($unite == null) {
				$query->bindValue(":unite", null, \PDO::PARAM_NULL);
			} else {
				$query->bindParam(":unite", $unite);
			}

			if($qualite == null) {
				$query->bindValue(":qualite", null, \PDO::PARAM_NULL);
			} else {
				$query->bindParam(":qualite", $qualite);
			}

			if($commentaire == null) {
				$query->bindValue(":commentaire", null, \PDO::PARAM_NULL);
			} else {
				$query->bindParam(":commentaire", $commentaire);
			}

			if($idPrevisionnel == null) {
				$query->bindValue(":idPrevisionnel", null, \PDO::PARAM_NULL);
			} else {
				$query->bindParam(":idPrevisionnel", $idPrevisionnel);
			}
			
			$query->bindParam(":idEmplacementReserve", $idEmplacementReserve);
			return $query->execute();
		}
	}

	public function updateSuivi($prep_sol, $lutte_adv, $semis, $plantation, $debutRecolte, $finRecolte, $quantite, $unite,$qualite,$commentaire, $idSuivi) {
		$query = $this->pdo->prepare('UPDATE Suivi SET preparationSol = :preparationSol ,lutteAdventices = :lutteAdventices, semis = :semis,plantation = :plantation, debutRecolte = :debutRecolte, finRecolte = :finRecolte, quantite = :quantite, unite = :unite, qualite = :qualite, commentaire = :commentaire WHERE idSuivi = :idSuivi');
		if($prep_sol == null) {
			$query->bindValue(":preparationSol", null, \PDO::PARAM_NULL);
		} else {
			$query->bindParam(":preparationSol",$prep_sol);
		}

		if(is_null($lutte_adv)) {
			$query->bindValue(":lutteAdventices", null, \PDO::PARAM_NULL);
		} else {
			$query->bindParam(":lutteAdventices",$lutte_adv);
		}

		if($semis == null) {
			$query->bindValue(":semis", null, \PDO::PARAM_NULL);
		} else {
			$query->bindParam(":semis", $semis);
		}

		if($plantation == null) {
			$query->bindValue(":plantation", null, \PDO::PARAM_NULL);
		} else {
			$query->bindParam(":plantation", $plantation);
		}

		if($debutRecolte == null) {
			$query->bindValue(":debutRecolte", null, \PDO::PARAM_NULL);
		} else {
			$query->bindParam(":debutRecolte", $debutRecolte);
		}

		if($finRecolte == null) {
			$query->bindValue(":finRecolte", null, \PDO::PARAM_NULL);
		} else {
			$query->bindParam(":finRecolte", $finRecolte);
		}

		if($quantite == null) {
			$query->bindValue(":quantite", null, \PDO::PARAM_NULL);
		} else {
			$query->bindParam(":quantite", $quantite);
		}

		if($unite == null) {
			$query->bindValue(":unite", null, \PDO::PARAM_NULL);
		} else {
			$query->bindParam(":unite", $unite);
		}

		if($qualite == null) {
			$query->bindValue(":qualite", null, \PDO::PARAM_NULL);
		} else {
			$query->bindParam(":qualite", $qualite);
		}
		if($commentaire == null) {
			$query->bindValue(":commentaire", null, \PDO::PARAM_NULL);
		} else {
			$query->bindParam(":commentaire", $commentaire);
		}

		$query->bindParam(":idSuivi", $idSuivi);
		return $query->execute();
	}

	public function getAllSuivi($idEmplacementReserve) {
		$req = $this->pdo->prepare('SELECT * FROM suivi WHERE idEmplacementReserve=?');
		$req->execute(array($idEmplacementReserve));

		$retour = array();
		if ($req->rowCount()) {
			$result = $req->fetchAll();
			foreach ($result as $key => $suivi) {
				if($suivi['preparationSol'] != null)
					$retour[$key]['preparationSol'] = date("d/m/Y", strtotime($suivi['preparationSol']));
				
				if($suivi['lutteAdventices'] != null)
					$retour[$key]['lutteAdventices'] = date("d/m/Y", strtotime($suivi['lutteAdventices']));
				
				if($suivi['semis'] != null)
					$retour[$key]['semis'] = date("d/m/Y", strtotime($suivi['semis']));

				if($suivi['plantation'] != null)
					$retour[$key]['plantation'] = date("d/m/Y", strtotime($suivi['plantation']));

				if($suivi['debutRecolte'] != null)
					$retour[$key]['debutRecolte'] = date("d/m/Y", strtotime($suivi['debutRecolte']));

				if($suivi['finRecolte'])
					$retour[$key]['finRecolte'] = date("d/m/Y", strtotime($suivi['finRecolte']));

				$retour[$key]['quantite'] = $suivi['quantite'];
				$retour[$key]['idSuivi'] = $suivi['idSuivi'];
				$retour[$key]['unite'] = $suivi['unite'];
				$retour[$key]['idPrevisionnel'] = $suivi['idPrevisionnel'];
			}
			return $retour;
		}	
		else return null;

		
	}

	public function deleteSuivi($idSuivi) {
		$sql = "DELETE FROM Suivi WHERE idSuivi = :idSuivi";
		$query = $this->pdo->prepare($sql);
		$query->bindParam(":idSuivi", $idSuivi);
		return $query->execute();
	}
	
	/**
	 * Gestion des tâches
	 */
	public function getCode($idEmplacementReserve) {
		//Récupération de l'emplacement réservé
		$sql = "SELECT * FROM EmplacementReserve WHERE idEmplacementReserve = ?";
		$query = $this->pdo->prepare($sql);
		$query->execute(array($idEmplacementReserve));
		$emplacementR = $query->fetch();
		
		//Récupération du code légume
		$sqlCodeLegume = "SELECT codeLegume FROM Variete WHERE idVariete = ?";
		$queryCodeLegume = $this->pdo->prepare($sqlCodeLegume);
		$queryCodeLegume->execute(array($emplacementR['idVariete']));
		$legume = $queryCodeLegume->fetch();

		$sqlCodeEmplacement = "SELECT codeEmplacement FROM Emplacement WHERE idEmplacement=?";
		$queryCodeEmplacement = $this->pdo->prepare($sqlCodeEmplacement);
		$queryCodeEmplacement->execute(array($emplacementR['idEmplacement']));
		$emplacement = $queryCodeEmplacement->fetch();

		return array("codeLegume" => $legume['codeLegume'], "codeEmplacement" => $emplacement['codeEmplacement']);
	}

	public function getLegumeVarieteCommentaireByPrev($idPrev) {
		$tab = array();

		$query = $this->pdo->prepare("SELECT variete.* FROM variete, previsionnel, emplacementreserve WHERE emplacementreserve.idEmplacementReserve = previsionnel.idEmplacementReserve AND emplacementreserve.idVariete = variete.idVariete AND previsionnel.idPrevisionnel = :id");
		$query->bindParam(":id", $idPrev);
		$query->execute();
		$dataVariete = $query->fetch();

		$query2 = $this->pdo->prepare("SELECT legume.* FROM legume WHERE legume.idLegume = :id2");
		$query2->bindParam(":id2", $dataVariete['idLegume']);
		$query2->execute();
		$dataLegume = $query2->fetch();

		array_push($tab, $dataLegume['nom']);
		array_push($tab, $dataVariete['nom']);
		array_push($tab, $dataVariete['commentaire']);

		return $tab;
	}

	//FUNCTIONS PREVISIONNEL DES TACHES HEBDOMADAIRE

	public function getSemisPrevisionnelTache($week) {
		$sql = "SELECT * FROM Previsionnel WHERE semis IN (?,?,?,?,?,?,?)";
		$query = $this->pdo->prepare($sql);
		$query->execute($week);
		
		return $query->fetchAll();
	}

	public function getPlantationPrevisionnelTache($week) {
		$sql = "SELECT * FROM Previsionnel WHERE plantation IN (?,?,?,?,?,?,?)";
		$query = $this->pdo->prepare($sql);
		$query->execute($week);
		
		return $query->fetchAll();
	}

	public function getRecoltePrevisionnelTache($week) {
		$sql = "SELECT * FROM previsionnel WHERE debutRecolte <= :dimanche AND finRecolte >= :lundi";
		$query = $this->pdo->prepare($sql);
		$query->bindParam(":lundi",$week[0]);
		$query->bindParam(":dimanche",$week[6]);
		$query->execute();
		return $query->fetchAll();
	}

	//FUNCTIONS SUIVI DES TACHES HEBDOMADAIRE

	public function getSemisSuiviTache($week) {
		$sql = "SELECT * FROM suivi WHERE semis IN (?,?,?,?,?,?,?)";
		$query = $this->pdo->prepare($sql);
		$query->execute($week);
		
		return $query->fetchAll();
	}

	public function getPlantationSuiviTache($week) {
		$sql = "SELECT * FROM suivi WHERE plantation IN (?,?,?,?,?,?,?)";
		$query = $this->pdo->prepare($sql);
		$query->execute($week);
		
		return $query->fetchAll();
	}

	public function getRecolteSuiviTache($week) {
		$sql = "SELECT * FROM suivi WHERE debutRecolte <= :dimanche";
		$query = $this->pdo->prepare($sql);
		$query->bindParam(":dimanche",$week[6]);
		$query->execute();
		return $query->fetchAll();
	}

	//FUNCTIONS TACHES EN RETARD

	public function getSemisPrevTacheRetard($week) {
		$sql = "SELECT previsionnel.* FROM previsionnel WHERE previsionnel.semis < :first AND previsionnel.idPrevisionnel NOT IN (SELECT idPrevisionnel FROM suivi WHERE suivi.semis IS NOT NULL)";
		$query = $this->pdo->prepare($sql);
		$query->bindParam(":first", $week[0]);
		$query->execute();
		return $query->fetchAll();
	}

	public function getPlantationPrevTacheRetard($week) {
		$sql = "SELECT previsionnel.* FROM previsionnel WHERE previsionnel.plantation < :first AND previsionnel.idPrevisionnel NOT IN (SELECT idPrevisionnel FROM suivi WHERE suivi.plantation IS NOT NULL)";
		$query = $this->pdo->prepare($sql);
		$query->bindParam(":first", $week[0]);
		$query->execute();
		return $query->fetchAll();
	}

	public function getRecoltePrevTacheRetard($week) {
		$sql = "SELECT previsionnel.* FROM previsionnel WHERE previsionnel.finRecolte < :first AND previsionnel.idPrevisionnel NOT IN (SELECT idPrevisionnel FROM suivi WHERE suivi.finRecolte IS NOT NULL)";
		$query = $this->pdo->prepare($sql);
		$query->bindParam(":first", $week[0]);
		$query->execute();
		return $query->fetchAll();
	}

	public function getAllVarietesByLegume($idLegume) {
		$query = $this->pdo->prepare("SELECT * FROM variete WHERE idLegume = :id");
		$query->bindParam(":id", $idLegume);
		$query->execute();
		return $query->fetchAll();
	}


	/**
	 * Récupère la liste des légumes et de ses variétés
	 */
	public function getAllVarietes($idLegume=null) {
		if(is_null($idLegume)) {
			$sql = "SELECT Variete.idVariete, Legume.nom as nomLegume, Variete.nom, Variete.codeLegume, Variete.commentaire, Semencier.nom as semencier FROM Legume,Variete,Semencier WHERE Legume.idLegume = Variete.idLegume AND Variete.idSemencier = Semencier.idSemencier";
			$query = $this->pdo->prepare($sql);
		} else {
			$sql = "SELECT Variete.idVariete, Legume.nom as nomLegume, Variete.nom, Variete.codeLegume, Variete.commentaire, Semencier.nom as semencier FROM Legume,Variete,Semencier WHERE Legume.idLegume = Variete.idLegume AND Variete.idSemencier = Semencier.idSemencier AND Legume.idLegume=:idLegume";
			$query = $this->pdo->prepare($sql);
			$query->bindParam(":idLegume",$idLegume);
		}		
        $query->execute();
        return $query->fetchAll();
	}
}
