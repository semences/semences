<?php
/**
 * Sauvegarde MySQL
 *
 * @package	BackupMySQL
 * @author	Benoit Asselin <contact@ab-d.fr>
 * @version	backup.php, 2013/01/13
 * @link	http://www.ab-d.fr/
 *
 */
namespace Classe;

/**
 * Sauvegarde MySQL
 */
class BackupMySQL extends \mysqli {
	
	/**
	 * Dossier des fichiers de sauvegardes
	 * @var string
	 */
	protected $dossier;
	
	/**
	 * Nom du fichier
	 * @var string
	 */
	protected $nom_fichier;
	
	
	/**
	 * Constructeur
	 * @param array $options
	 */
	public function __construct($nomfile, $options = array()) {
		$default = array(
			'host' => ini_get('mysqli.default_host'),
			'username' => ini_get('mysqli.default_user'),
			'passwd' => ini_get('mysqli.default_pw'),
			'dbname' => '',
			'port' => ini_get('mysqli.default_port'),
			'socket' => ini_get('mysqli.default_socket'),
			// autres options
			'nbr_fichiers' => 5,
			'nom_fichier' => $nomfile
			);
		$options = array_merge($default, $options);
		extract($options);
		
		// Connexion de la connexion DB
		@parent::__construct($host, $username, $passwd, $dbname, $port, $socket);
		if($this->connect_error) {
			$this->message('Erreur de connexion (' . $this->connect_errno . ') '. $this->connect_error);
			return;
		}
		
		// Controle du fichier
		$this->nom_fichier = $nom_fichier;
	}
	
	/**
	 * Protection des quot SQL
	 * @param string $string
	 * @return string
	 */
	protected function insert_clean($string) {
		// Ne pas changer l'ordre du tableau !!!
		$s1 = array( "\\"	, "'"	, "\r", "\n", );
		$s2 = array( "\\\\"	, "''"	, '\r', '\n', );
		return str_replace($s1, $s2, $string);
	}
	
	/**
	 * Sauvegarder les tables dans un fichier .sql
	 */
	public function exportSql() {
		$fichierDump = fopen($this->nom_fichier, "wb");
		
		$sql  = '--' ."\n";
		$sql .= '-- '. $this->nom_fichier ."\n";
		$sql .= "SET FOREIGN_KEY_CHECKS=0;\n";
		$sql .= "CREATE DATABASE IF NOT EXISTS seed;\n";
		$sql .= "USE seed;\n";
		fwrite($fichierDump, $sql);


		
		// Liste les tables
		$result_tables = $this->query('SHOW TABLE STATUS');
		if($result_tables && $result_tables->num_rows) {
			while($obj_table = $result_tables->fetch_object()) {
				
				// DROP ...
				$sql  = "\n\n";
				$sql .= 'DROP TABLE IF EXISTS `'. $obj_table->{'Name'} .'`' .";\n";

				// CREATE ...
				$result_create = $this->query('SHOW CREATE TABLE `'. $obj_table->{'Name'} .'`');
				if($result_create && $result_create->num_rows) {
					$obj_create = $result_create->fetch_object();
					$sql .= $obj_create->{'Create Table'} .";\n";
					$result_create->free_result();
				}

				// INSERT ...
				$result_insert = $this->query('SELECT * FROM `'. $obj_table->{'Name'} .'`');
				if($result_insert && $result_insert->num_rows) {
					$sql .= "\n";
					while($obj_insert = $result_insert->fetch_object()) {
						$virgule = false;
						
						$sql .= 'INSERT INTO `'. $obj_table->{'Name'} .'` VALUES (';
						foreach($obj_insert as $val) {
							$sql .= ($virgule ? ',' : '');
							if(is_null($val)) {
								$sql .= 'NULL';
							} else {
								$sql .= '\''. $this->insert_clean($val) . '\'';
							}
							$virgule = true;
						} // for
						
						$sql .= ')' .";\n";
						
					} // while
					$result_insert->free_result();
				}
				
				fwrite($fichierDump, $sql);
			} // while
			$result_tables->free_result();
		}

		$sql .= "\n\n\nSET FOREIGN_KEY_CHECKS=1;\n";

		fwrite($fichierDump, $sql);
		fclose($fichierDump);
	}

	/**
	 * Importation des données provenant d'un fichier .sql
	 */
	public function importSql() {

		// Temporary variable, used to store current query
		$templine = '';
		// Read in entire file
		$lines = file($this->nom_fichier);
		// Loop through each line
		foreach ($lines as $line)
		{
			// Skip it if it's a comment
			if (substr($line, 0, 2) == '--' || $line == '')
			    continue;

			// Add this line to the current segment
			$templine .= $line;
			// If it has a semicolon at the end, it's the end of the query
			if (substr(trim($line), -1, 1) == ';')
			{
			    // Perform the query
			    $this->query($templine);
			    // Reset temp variable to empty
			    $templine = '';
			}
		}
	}
	
}



?>