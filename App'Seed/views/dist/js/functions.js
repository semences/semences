$( function() {

	var loader = $("#loader");

	setTimeout(function() {
      loader.fadeOut('slow');
	}, 3000);

	//popover principal de la page
	$.ajax({
			type: "POST",
			dataType: "json",
			url: "/taches/layoutPopover"
		}).done(function(data) {
			$("#popover-notif span").text(data[0]);
			$('#popover-notif').popover({
				'content':'Vous avez '+data[0]+' tâches cette semaine dont '+data[1]+' en retard',
				'html':true,
				'title':'<a href="/taches">Notifications</a>',
			});
		});

	//animation fleche au niveau du li "gestion"
	var target = $("li a[data-target='#demo']");
	targetI= $("li a[data-target='#demo'] i.fa-caret-down");
	
	target.on("click", function(){
		if(targetI.hasClass("fa-caret-down"))
		{
			targetI.removeClass("fa-caret-down").addClass("fa-caret-up");
		}else{
			targetI.removeClass("fa-caret-up").addClass("fa-caret-down");
		}
	});

	//nav-pills pour les datas
	$("#datapills li").click(function() { 
		//on remove la class active de tout les liens, et on l'ajoute a celui selectionné
		$("#datapills li").each(function() {
			$(this).removeClass("active");
		});
		
		$(this).addClass("active");

		//on n'affiche aucune table
		$("#tableData table").each(function() {
			$(this).css("display", "none");
		});

		//on recupère la table correspondante au lien et on l'affiche
		var datalist = $(this).text();

		var location = "#data"+datalist;
		console.log(location);

		$(location).toggle("slow");
	});


	$(".datepicker").datepicker({
		language: "fr",
		calendarWeeks: true,
	    format: 'dd/mm/yyyy',
	    orientation: "auto left",
    	autoclose: true,
	});

});