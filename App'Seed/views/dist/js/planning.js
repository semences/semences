﻿/* Permet L'affichage du gantt général sur la page planning */
$('.viewGantt').click(function(){
	$.ajax({
		type: "POST",
		dataType: "json",
		url: "/planning/get/getGantt",
	}).done(function(data) {
		var data_gantt = [];
		data_gantt= '[';
		if($.isEmptyObject(data)) {
			var html = "Aucune prévision trouvée lié aux emplacements et variétés";
		}else{
			var $i = 1;
			$.each(data, function(index,element) {
				var oneDay=((60*24*24)*1000);
				semis = element['semis'];
				semis_1 = semis.substring(0, semis.indexOf('/'));
				semis_2 = semis.substring(3, 5);
				semis_3 = semis.substring(6, semis.length);
			 
				var SemisDeb = new Date(semis_3,semis_2-1,semis_1);
			
				plantation = element['plantation'];
				plantation_1 = plantation.substring(0, plantation.indexOf('/'));
				plantation_2 = plantation.substring(3, 5);
				plantation_3 = plantation.substring(6, plantation.length);
				var SemisFin = new Date(plantation_3,plantation_2-1,plantation_1);
			
				RecolteDeb = element['debutRecolte'];
				RecolteDeb_1 = RecolteDeb.substring(0, RecolteDeb.indexOf('/'));
				RecolteDeb_2 = RecolteDeb.substring(3, 5);
				RecolteDeb_3 = RecolteDeb.substring(6, RecolteDeb.length);
				var DebRecolte = new Date(RecolteDeb_3,RecolteDeb_2-1,RecolteDeb_1);
			
				RecolteFin = element['finRecolte'];
				RecolteFin_1 = RecolteFin.substring(0, RecolteFin.indexOf('/'));
				RecolteFin_2 = RecolteFin.substring(3, 5);
				RecolteFin_3 = RecolteFin.substring(6, RecolteFin.length);
				var RecolteFin = new Date(RecolteFin_3,RecolteFin_2-1,RecolteFin_1);
			
				data_gantt = data_gantt+"{'name': '"+element['codeLegume']+"','desc': '"+ element['codeEmplacement']+"' , 'values' : ["   ;
				data_gantt=data_gantt+" {'id': 'b"+$i+"','from':'/Date("+ SemisDeb +")/', 'to': '/Date("+(SemisFin - oneDay)+")/' ,'label':'Semis' , 'customClass': 'ganttYellow'}, {'from':'/Date("+ SemisFin +")/', 'to': '/Date("+(DebRecolte- oneDay) +")/' ,'desc': 'Id: 4<br/>Nom:   Plantation','label':'Plantation' , 'customClass': 'ganttGreen' }, {'from':'/Date("+ DebRecolte +")/', 'to': '/Date("+(RecolteFin- oneDay) +")/','label':'Récolte' , 'customClass': 'ganttRed'}]},";	
				
				$i=$i+1;
			});
			data_gantt=data_gantt+']';
			var obj=eval("("+data_gantt+")");
			$(".gantt").gantt({
			source : obj,
					navigate: "scroll",
					maxScale: "weeks",
					itemsPerPage: 10
				});
			$(".gantt").show();
			
		};
	});
})
