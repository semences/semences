$( function() {
	//initialisation des chevrons avec des routes 

	$("#semaineTaches > h4:nth-child(1) > a:nth-child(2)").attr('href', '/taches/'+parseInt($("#semaineTaches span.label:nth-child(1)").text())+'-'+parseInt(parseInt($("#semaineTaches span.label:nth-child(3)").text())-1));
	$("#semaineTaches > h4:nth-child(1) > a:nth-child(4)").attr('href', '/taches/'+parseInt($("#semaineTaches span.label:nth-child(1)").text())+'-'+parseInt(parseInt($("#semaineTaches span.label:nth-child(3)").text())+1));
	
	//click sur les chevrons 

	$("#semaineTaches > h4:nth-child(1) > a:nth-child(2)").click(function() {
		if(parseInt($("#semaineTaches span.label:nth-child(3)").text()) > 1) {
			$("#semaineTaches span.label:nth-child(3)").text(parseInt(parseInt($("#semaineTaches span.label:nth-child(3)").text())-1));
		}
		else if(parseInt($("#semaineTaches span.label:nth-child(3)").text()) == 1){
			console.log('lolilol');
			$("#semaineTaches span.label:nth-child(1)").text(parseInt(parseInt($("#semaineTaches span.label:nth-child(1)").text())-1));
			$("#semaineTaches span.label:nth-child(3)").text("52");
		}
		$("#semaineTaches > h4:nth-child(1) > a:nth-child(2)").attr('href', '/taches/'+parseInt($("#semaineTaches span.label:nth-child(1)").text())+'-'+parseInt($("#semaineTaches span.label:nth-child(3)").text()));
		$("#semaineTaches > h4:nth-child(1) > a:nth-child(4)").attr('href', '/taches/'+parseInt($("#semaineTaches span.label:nth-child(1)").text())+'-'+parseInt($("#semaineTaches span.label:nth-child(3)").text()));
	});

	$("#semaineTaches > h4:nth-child(1) > a:nth-child(4)").click(function() {
		if(parseInt($("#semaineTaches span.label:nth-child(3)").text()) < 52) {
			$("#semaineTaches span.label:nth-child(3)").text(parseInt(parseInt($("#semaineTaches span.label:nth-child(3)").text())+1));
		}
		else {
			$("#semaineTaches span.label:nth-child(1)").text(parseInt(parseInt($("#semaineTaches span.label:nth-child(1)").text())+1));
			$("#semaineTaches span.label:nth-child(3)").text("01");
		}
		$("#semaineTaches > h4:nth-child(1) > a:nth-child(2)").attr('href', '/taches/'+parseInt($("#semaineTaches span.label:nth-child(1)").text())+'-'+parseInt($("#semaineTaches span.label:nth-child(3)").text()));
		$("#semaineTaches > h4:nth-child(1) > a:nth-child(4)").attr('href', '/taches/'+parseInt($("#semaineTaches span.label:nth-child(1)").text())+'-'+parseInt($("#semaineTaches span.label:nth-child(3)").text()));
	});

	//POPOVER POUR LES TACHES
	$("#taches form label").each(function() {
		var thiss = $(this);

		$.ajax({
			type: "POST",
			dataType: "json",
			data: {"idPrev": $(this).parent().children("input[type=hidden]").val()},
			url: "/taches/infoPopover"
		}).done(function(data) {
			var legume = data[0];
			var variete = data[1];
			var commentaire;
			if(data[2].length != 0)
				commentaire = '<br>Commentaire : ' + data[2];
			else commentaire = "";

			thiss.popover({
				'content': legume + ' ' + variete + commentaire,
				'html':true,
				'title':'Info légume',
				'trigger': "hover",
				'placement': "bottom"
			});
		});
	});

	//MASQUAGE DE TACHES
	//taches enregistrées
	$("#semaineTaches #tacheMaskSaved").click(function() {
		if($("#tacheMaskSaved i").hasClass("fa-toggle-off")) {
			$("#tacheMaskSaved i").removeClass("fa-toggle-off");
			$("#tacheMaskSaved i").addClass("fa-toggle-on");
		}
		else {
			$("#tacheMaskSaved i").removeClass("fa-toggle-on");
			$("#tacheMaskSaved i").addClass("fa-toggle-off");
		}
		$("#taches .suiviTache").toggle("slow");
	});

	//taches en retard
	$("#semaineTaches #tacheMaskRetard").click(function() {
		if($("#tacheMaskRetard i").hasClass("fa-toggle-off")) {
			$("#tacheMaskRetard i").removeClass("fa-toggle-off");
			$("#tacheMaskRetard i").addClass("fa-toggle-on");
		}
		else {
			$("#tacheMaskRetard i").removeClass("fa-toggle-on");
			$("#tacheMaskRetard i").addClass("fa-toggle-off");
		}
		$("#taches .retardTache").toggle("slow");
	});

});