$(".semencierDelete").click(function(){
	$("#idSemencierDelete").val($(this).attr("idSemencier"));
});

$(".semencierEdit").click(function(){
	$("#idSemencierEdit").val($(this).attr("idSemencier"));

	$.ajax({
		type: "POST",
		dataType: "json",
		data: {"idSemencier":$(this).attr("idSemencier")},
		url: "/semencier/get"
	}).done(function(data) {
		$("#semencierFormEdit input[name='nomSemencier']").val(data['nom']);
	});
});