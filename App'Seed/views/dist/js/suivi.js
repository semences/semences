$(".deleteSuivi").click(function() {
	$("#idSuiviDelete").val($(this).attr("idSuivi"));
});

$(".editSuivi").click(function() {
	$("#editSuivi input[name='idSuivi']").val($(this).attr("idSuivi"));
	$.ajax({
		type: "post",
		dataType: "json",
		url: "/planning/suivi/get",
		data: {'idSuivi': $(this).attr("idSuivi")}
	}).done(function(data){
		$("#editSuivi input[name='preparationSol']").val(data['preparationSol']);
		$("#editSuivi input[name='lutteAdventices']").val(data['lutteAdventices']);
		$("#editSuivi input[name='semis']").val(data['semis']);
		$("#editSuivi input[name='plantation']").val(data['plantation']);
		$("#editSuivi input[name='debutRecolte']").val(data['debutRecolte']);
		$("#editSuivi input[name='finRecolte']").val(data['finRecolte']);
		$("#editSuivi input[name='quantite']").val(data['quantite']);
		$("#editSuivi input[name='unite']").val(data['unite']);
		$("#editSuivi input[name='idSuivi']").val(data['idSuivi']);
		$("#editSuivi input[name='qualite']").val(data['qualite']);
		$("#editSuivi textarea[name='commentaire']").val(data['commentaire']);
	});
}); 