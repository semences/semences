$(".emplacementDelete").click(function(){
	$("#idEmplacementDelete").val($(this).attr("idEmplacement"));
});

$(".emplacementEdit").click(function() {
	$("#idEmplacementEdit").val($(this).attr("idEmplacement"));
	$.ajax({
		dataType: "json",
		type: "post",
		data: {"idEmplacement":$(this).attr("idEmplacement")},
		url: "/emplacement/get"
	}).done(function(data){
		console.log(data);
		$("#emplacementFormEdit input[name='parcelle']").val(data['parcelle']);
		$("#emplacementFormEdit input[name='carreCulture']").val(data['carreCulture']);
		$("#emplacementFormEdit input[name='planche']").val(data['planche']);
		$("#emplacementFormEdit input[name='codeEmplacement']").val(data['codeEmplacement']);
		$("#emplacementFormEdit textarea[name='commentaire']").val(data['commentaire']);
	});
});