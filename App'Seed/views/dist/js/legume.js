﻿/* Filtre de la page liste des légumes */
var filtre = $("#filtreVariete");
var varieteTab = $("#varietesList");
//select les plus-square
var legumePlus = $("#legumeVariete .viewVariete");
$(".formAddVariete").hide();

filtre.change(function() {
	var id = $(this).val();
	$.ajax({
		type: "POST",
		dataType: "json",
		url: "/legume/filter",
		data: {"idLegume": id}
	}).done(function(data) {
		var html="<tr><th>Nom du légume</th><th>Variété</th><th>Code légume</th><th>Semencier</th><th>Commentaire</th><th>Action</th></tr>";
		if($.isEmptyObject(data)) {
			html = html+"<tr><td>Aucune variété trouvée</td></tr>";
		} else {
			$.each(data, function(index,element) {
				var row = "<tr>";
				row = row+"<td>"+element['nomLegume']+"</td>";
				row = row+"<td>"+element['nom']+"</td>";
				row = row+"<td>"+element['codeLegume']+"</td>";
				row = row+"<td>"+element['semencier']+"</td>";
				row = row+"<td>"+element['commentaire']+"</td>";
				row = row+"<td><button type='button' title='Éditer' idVariete='"+element['idVariete']+"' class='btn btn-default varieteEdit' data-toggle='modal' data-target='#editVariete'> <i class='fa fa-pencil fa-fw'></i></button> <button type='button' title='Supprimer' idVariete='"+element['idVariete']+"' class='btn btn-danger varieteDelete' data-toggle='modal' data-target='#deleteVariete'><i class='fa fa-trash-o fa-lg'></i></button></td>"
				row = row+"</tr>";
				html = html+row;
			});
		} 
		varieteTab.html(html);
	});
});
			

/*permettant de faire un tableau des variétés directement dans le tableau des légumes*/
$("tr.LegumeVariete").delegate('i','click',function() {
	var id = $(this).attr("idLegume");
	var id_tab = $(this).attr("count");
	$.ajax({
		type: "POST",
		dataType: "json",
		url: "/legume/filter",
		data: {"idLegume": id}
	}).done(function(data) {
		var html="";
			
		if($.isEmptyObject(data)) {
			var row = "<tr class='newtr'>";
			row = row+"<td>Il n'y a pas de variétés pour ce légume</td>";
			html = html+row;
				var plus = $("tr.LegumeVariete").eq(id_tab-1).find("i").eq(0);
				if(plus.hasClass("fa-plus-square")){
					plus.removeClass("fa-plus-square");
					plus.addClass("fa-minus-square");
					$("tr.LegumeVariete").eq(id_tab-1).after(html);
					$("input#idLegume").eq(id_tab-1).attr("value", plus.attr("idLegume"));
					$(".formAddVariete").eq(id_tab-1).show();
				}else{
					plus.removeClass("fa-minus-square");
					plus.addClass("fa-plus-square");
					$("tr.LegumeVariete ~ tr.newtr").remove();
					$(".formAddVariete").eq(id_tab-1).hide();
				}
		}else{
			$.each(data, function(index,element) {
				var row = "<tr class='newtr'>";
				row = row+"<td>"+element['nom']+"</td>";
				row = row+"<td>"+ $("tr.LegumeVariete").eq(id_tab-1).find("td:eq(1)").html() +"."+ element['codeLegume']+"</td>";
				row = row+"<td>"+element['semencier']+"</td>";
				row = row+"<td>"+element['commentaire']+"</td>";
				row = row+"<td><button type='button' title='Éditer' idVariete='"+element['idVariete']+"' class='btn btn-default varieteEdit' data-toggle='modal' data-target='#editVariete'> <i class='fa fa-pencil fa-fw'></i></button> <button type='button' title='Supprimer' idVariete='"+element['idVariete']+"' class='btn btn-danger varieteDelete' data-toggle='modal' data-target='#deleteVariete'><i class='fa fa-trash-o fa-lg'></i></button></td>"
				row = row+"</tr>";
				html = html+row;
			});

				var plus = $("tr.LegumeVariete").eq(id_tab-1).find("i").eq(0);
				
				if(plus.hasClass("fa-plus-square")){
					plus.removeClass("fa-plus-square");
					plus.addClass("fa-minus-square");
					$("tr.LegumeVariete").eq(id_tab-1).after(html);
					$("input#idLegume").eq(id_tab-1).attr("value", plus.attr("idLegume"));
					$(".formAddVariete").eq(id_tab-1).show();
				}else{
					plus.removeClass("fa-minus-square");
					plus.addClass("fa-plus-square");
					$("tr.LegumeVariete").eq(id_tab-1).nextUntil($(".formAddVariete").eq(id_tab-1)).remove();
					$(".formAddVariete").eq(id_tab-1).hide();
				}
		}
			
	});	
});

// /* génération de la liste déroulante pour ajouter une variété directement dans le tableau des légumes */
// $(document).on('click', 'tr.LegumeVariete i',function() {
	// $.ajax({
			// type: "POST",
			// dataType: "json",
			// url: "/semencier/getAll",
			// error : function(resultat, statut, erreur){
				// console.log(erreur);
			// }
		// }).done(function(data) {
			// html="";
			
			// html=html+'<select class="form-control" name="idSemencier" required><option value="">Sélectionner un semencier</option>';
				// $.each(data, function(index,element) {
					// html=html+'<option value="'+element['idSemencier']+'">'+element['nom']+'</option>'
				// })
			// html=html+'</select>';
			// $(".formAddVariete td:eq(2)").html(html);
		// });
	
// });


/* Génération d'une liste des variétés en fonction du légume choisi dans la page Planning */
$("#legumeSelect").change(function(){
	var id = $(this).val();
	if(id != "") {
		$.ajax({
			type: "POST",
			dataType: "json",
			url: "/legume/filter",
			data: {"idLegume": id}
		}).done(function(data) {
			var html = "";
			if($.isEmptyObject(data)) {
				html = "<p>Aucune variété trouvée</p>";
			} else {
				html = "<label for='legume'>Liste des variétés</label><select class='form-control' name='idVariete' required><option value=''>Sélectionner une variété</option>";
				$.each(data, function(index,element) {
					var row = "<option value='"+element['idVariete']+"'>"+element['nom']+"</option>";
					html = html+row;
				});
				html = html+"</select>";
			}

			$("#ListeVariete").html(html);
		});
	} else {
		$("#ListeVariete").html("");
	}
});

/* Modification d'un légume */
$(".legumeEdit").click(function() {
	var idLegume = $(this).attr("idLegume");
	$("#idLegumeEdit").val(idLegume);
	$.ajax({
		type: "POST",
		dataType: "json",
		url: "/legume/get",
		data: {"idLegume": idLegume}
	}).done(function(data) {
		$("#legumeFormEdit input[name='nomLegume']").val(data['nom']);	
	});
});

/* Suppression de légume */
$(".legumeDelete").click(function() {
	var idLegume = $(this).attr("idLegume");
	$("#idLegumeDelete").val(idLegume);
});

/* Modification de variété */
$("body").delegate('tr.newtr .varieteEdit','click',function(){
	var idVariete = $(this).attr("idVariete");
	$("#idVarieteEdit").val(idVariete);
	$.ajax({
		type: "POST",
		dataType: "json",
		data: {"idVariete":idVariete},
		url: "/legume/variete/get" 
	}).done(function(data) {
		$("#varieteFormEdit select[name='idLegume']").val(data['idLegume']);
		$("#varieteFormEdit input[name='nom']").val(data['nom']);
		$("#varieteFormEdit input[name='codeLegume']").val(data['codeLegume']);
		$("#varieteFormEdit textarea[name='commentaire']").val(data['commentaire']);
		$("#varieteFormEdit select[name='idSemencier']").val(data['idSemencier']);
	});
});

/* Suppression d'une variété */
$("body").delegate('tr.newtr .varieteDelete','click',function(){
	var idVariete = $(this).attr("idVariete");
	$("#idVarieteDelete").val(idVariete);
});









