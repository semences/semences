var del = $(".deletePrevision");
var edit = $(".editPrevision");
var buttonGantt = $(".viewPrevision");

$('.gantt').hide();


del.click(function() {
	$("#idPrevisionDelete").val($(this).attr("idPrevision"));
});

edit.click(function() {
	$("#idPrevisionEdit").val($(this).attr("idPrevision"));
	$.ajax({
		type: "POST",
		dataType: "json",
		data: {"idPrevision": $(this).attr("idPrevision")},
		url: "/planning/prevision/get"
	}).done(function(data) {
		$("#editFormPrevision input[name='preparationSol']").val(data['preparationSol']);
		$("#editFormPrevision input[name='lutteAdventices']").val(data['lutteAdventices']);
		$("#editFormPrevision input[name='semis']").val(data['semis']);
		$("#editFormPrevision input[name='plantation']").val(data['plantation']);
		$("#editFormPrevision input[name='debutRecolte']").val(data['debutRecolte']);
		$("#editFormPrevision input[name='finRecolte']").val(data['finRecolte']);
	});
});

$('.gantt').hide();

$('.titre-gantt').hide();
buttonGantt.click(function() {
	$("#idPrevisionEdit").val($(this).attr("idPrevision"));
	var numberid = $(this).attr("numberId");
	$.ajax({
		type: "POST",
		dataType: "json",
		data: {"idPrevision": $(this).attr("idPrevision")},
		url: "/planning/prevision/get"
	}).done(function(data) {
			
			var oneDay=(60*60*24)*1000;
			semis = data['semis'];
			semis_1 = semis.substring(0, semis.indexOf('/'));
			semis_2 = semis.substring(3, 5);
			semis_3 = semis.substring(6, semis.length);
			
			//date du début de semis, qui nous permet d'en déduire la préparation du sol et des luttes 
			var SemisDeb = new Date(semis_3,semis_2-1,semis_1);
			
			//date lutte adventices
			var lutte_adv = new Date(semis_3,semis_2-1,semis_1);
			lutte_adv.setTime(SemisDeb.getTime()-((7 * data['lutteAdventices'])* oneDay));
			
			//date prepa sol
			var prep_sol = new Date();
			prep_sol.setTime(lutte_adv.getTime()-((7 * data['preparationSol']) * oneDay ));
			
			plantation = data['plantation'];
			plantation_1 = plantation.substring(0, plantation.indexOf('/'));
			plantation_2 = plantation.substring(3, 5);
			plantation_3 = plantation.substring(6, plantation.length);
			var SemisFin = new Date(plantation_3,plantation_2-1,plantation_1);
			
		    RecolteDeb = data['debutRecolte'];
			RecolteDeb_1 = RecolteDeb.substring(0, RecolteDeb.indexOf('/'));
			RecolteDeb_2 = RecolteDeb.substring(3, 5);
			RecolteDeb_3 = RecolteDeb.substring(6, RecolteDeb.length);
			var DebRecolte = new Date(RecolteDeb_3,RecolteDeb_2-1,RecolteDeb_1);
			
			RecolteFin = data['finRecolte'];
			RecolteFin_1 = RecolteFin.substring(0, RecolteFin.indexOf('/'));
			RecolteFin_2 = RecolteFin.substring(3, 5);
			RecolteFin_3 = RecolteFin.substring(6, RecolteFin.length);
			var RecolteFin = new Date(RecolteFin_3,RecolteFin_2-1,RecolteFin_1);
			
			var data_gantt = [
				{ "name": " Prép Sol ","desc": " "  ,"values": [{"id": "b0", "from": "/Date("+ prep_sol +")/", "to": "/Date("+ (lutte_adv - oneDay)+")/", "desc": "Id: 0<br/>Nom:   Préparation du sol", "label": " Préparation Sol", "customClass": "ganttGray", "dep": "b1"}]},
				{ "name": " Luttes Adventices ","desc": ""  ,"values": [{"id": "b1", "from": "/Date("+ lutte_adv +")/", "to": "/Date("+ (SemisDeb - oneDay) +")/", "desc": "Id: 1<br/>Nom:   Luttes Adventices", "label": " Luttes Adventices", "customClass": "ganttBlue", "dep": "b2"}]},
				{ "name": " Semis ","desc": ""  ,"values": [{"id": "b2", "from": "/Date("+ SemisDeb +")/", "to": "/Date("+ (SemisFin - oneDay) +")/", "desc": "Id: 2<br/>Nom:   Semis", "label": " Semis", "customClass": "ganttYellow", "dep": "b3"}]},
				{ "name": " Plantation ","desc": ""  ,"values": [{"id": "b3", "from": "/Date("+ SemisFin +")/", "to": "/Date("+ (DebRecolte - oneDay) +")/", "desc": "Id: 3<br/>Nom:   Plantation", "label": " Plantation", "customClass": "ganttGreen", "dep": "b4"}]},
				{ "name": " Récolte ","desc": ""  ,"values": [{"id": "b4", "from": "/Date("+ DebRecolte +")/", "to": "/Date("+ (RecolteFin) +")/", "desc": "Id: 4<br/>Nom:   Récolte", "label": " Récolte", "customClass": "ganttRed", "dep": "b5"}]},
			];
			
			$(".gantt").gantt({
				source: data_gantt,
				scale: $(".gantt").attr("scale"),
				navigate: "scroll",
				maxScale: "weeks",
				itemsPerPage: 10
			 });
		
		$('.titre-gantt').html("Prévision n°"+numberid + " <button type='button' class='btn btn-default' id='ganttEchelle'>Changer Echelle</button>");	
		$('.titre-gantt').fadeIn(350);
		$('.gantt').fadeIn(350);
		
		var ganttEchelle = $('#ganttEchelle');
			ganttEchelle.click(function(){
				if($(".gantt").attr("scale") == "days"){
				
					$(".gantt").attr("scale","weeks");
					
					$(".gantt").gantt({
							source: data_gantt,
							scale: $(".gantt").attr("scale"),
							navigate: "scroll",
							maxScale: "weeks",
							itemsPerPage: 10
					});
				}else{
					$(".gantt").attr("scale","days");	
					
						$(".gantt").gantt({
								source: data_gantt,
								scale: $(".gantt").attr("scale"),
								navigate: "scroll",
								maxScale: "weeks",
								itemsPerPage: 10
						});
				}
			});
	});
});


$(".suivi").click(function() {
	$("#idPrevisionSuivi").val($(this).attr("idPrevision"));
	$.ajax({
		type: "post",
		dataType: "json",
		data: {'idPrevisionnel': $(this).attr("idPrevision"), 'idEmplacementReserve': $(this).attr("idEmplacementR")},
		url: "/planning/prevision/suivi/get"
	}).done(function(data) {
		console.log(data);
		$("#addSuivi input[name='preparationSol']").val(data['preparationSol']);
		$("#addSuivi input[name='lutteAdventices']").val(data['lutteAdventices']);
		$("#addSuivi input[name='semis']").val(data['semis']);
		$("#addSuivi input[name='plantation']").val(data['plantation']);
		$("#addSuivi input[name='debutRecolte']").val(data['debutRecolte']);
		$("#addSuivi input[name='finRecolte']").val(data['finRecolte']);
		$("#addSuivi input[name='quantite']").val(data['quantite']);
		$("#addSuivi input[name='unite']").val(data['unite']);
		$("#addSuivi input[name='qualite']").val(data['qualite']);
		$("#addSuivi textarea[name='commentaire']").val(data['commentaire']);
		$("#addSuivi input[name='idSuivi']").val(data['idSuivi']);
	});
});

