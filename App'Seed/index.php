<?php 
	date_default_timezone_set('Europe/Paris');
	$loader = include('vendor/autoload.php');
	$loader->add('', 'model');

	$app = new Silex\Application;
	$app->register(new Silex\Provider\UrlGeneratorServiceProvider());
	$app->register(new Silex\Provider\TwigServiceProvider(), array(
		'twig.path' => __DIR__.'/views',
	));
	$app->register(new Silex\Provider\SessionServiceProvider());

	//requetes et réponses http formulaire
	use Symfony\Component\HttpFoundation\Request; 
	use Symfony\Component\HttpFoundation\Response;

	//download et upload fichiers
	use Symfony\Component\HttpFoundation\StreamedResponse;
	use Symfony\Component\HttpFoundation\BinaryFileResponse;
	use Symfony\Component\HttpFoundation\ResponseHeaderBag;

	$app['model'] = new Classe\BDD
	(
		'127.0.0.1',  // Hôte
		'seed',    // Base de données
		'root',    // Utilisateur
		''     // Mot de passe
	);	


	//
	/////////ACCUEIL/////////
	//
	$app->match('/', function() use ($app) {
		return $app['twig']->render('home.html.twig');
	})->bind('home');

	//
	/////////TÂCHES/////////
	//
	$app->match('/taches', function() use ($app) {
		$today = date("Y-m-d");
		$date = new DateTime($today);
		$weekNumber = $date->format("W");
		$year = date("Y");
		$week = array();
		for($day=1; $day<=7; $day++)
		{
		    $week[] = date('Y-m-d', strtotime($year."W".$weekNumber.$day));
		}
		//var_dump($week);
		$taches = array();

		$taches['previsionnel'] = array();
		$taches['retard'] = array();
		$taches['suivi'] = array();

		//previsions et suivis
		$semisPrev = $app['model']->getSemisPrevisionnelTache($week);
		$plantationPrev = $app['model']->getPlantationPrevisionnelTache($week);
		$recoltePrev = $app['model']->getRecoltePrevisionnelTache($week);

		$tabSemisPrev = array();
		$tabSemisSuivi = array();
		$i = 0;
		foreach($semisPrev as $prev) {
			$semisSuivi = $app['model']->getSuiviByPrevisionnel($prev['idPrevisionnel']);
			if($semisSuivi['semis'] != null) {
				//récupération du code légume + code emplacement
				$tabSemisSuivi[$i]['code'] = $app['model']->getCode($prev['idEmplacementReserve']);
				$tabSemisSuivi[$i]['prevision'] = $semisSuivi;
			}
			else {
				//récupération du code légume + code emplacement
				$tabSemisPrev[$i]['code'] = $app['model']->getCode($prev['idEmplacementReserve']);
				$tabSemisPrev[$i]['prevision'] = $prev;
			}

			$i++;
		}

		$tabPlantationPrev = array();
		$tabPlantationSuivi = array();
		$i = 0;
		foreach($plantationPrev as $prev) {
			$plantationSuivi = $app['model']->getSuiviByPrevisionnel($prev['idPrevisionnel']);
			if($plantationSuivi['plantation'] != null) {
				//récupération du code légume + code emplacement
				$tabPlantationSuivi[$i]['code'] = $app['model']->getCode($prev['idEmplacementReserve']);
				$tabPlantationSuivi[$i]['prevision'] = $plantationSuivi;
			}
			else {
				
				//récupération du code légume + code emplacement
				$tabPlantationPrev[$i]['code'] = $app['model']->getCode($prev['idEmplacementReserve']);
				$tabPlantationPrev[$i]['prevision'] = $prev;
			}

			$i++;
		}

		$tabRecoltePrev = array();
		$tabRecolteSuivi = array();
		$i = 0;
		foreach($recoltePrev as $prev) {
			$recolteSuivi = $app['model']->getSuiviByPrevisionnel($prev['idPrevisionnel']);
			if($recolteSuivi['debutRecolte'] != null || $recolteSuivi['finRecolte'] != null) {
				//récupération du code légume + code emplacement
				$tabRecolteSuivi[$i]['code'] = $app['model']->getCode($prev['idEmplacementReserve']);
				$tabRecolteSuivi[$i]['prevision'] = $recolteSuivi;
			}
			else {
				//récupération du code légume + code emplacement
				$tabRecoltePrev[$i]['code'] = $app['model']->getCode($prev['idEmplacementReserve']);
				$tabRecoltePrev[$i]['prevision'] = $prev;
			}

			$i++;
		}

		array_push($taches['previsionnel'], $tabSemisPrev);
		array_push($taches['previsionnel'], $tabPlantationPrev);
		array_push($taches['previsionnel'], $tabRecoltePrev);

		array_push($taches['suivi'], $tabSemisSuivi);
		array_push($taches['suivi'], $tabPlantationSuivi);
		array_push($taches['suivi'], $tabRecolteSuivi);


		//previsions en retard
		$semisRetard = $app['model']->getSemisPrevTacheRetard($week);
		$plantationRetard = $app['model']->getPlantationPrevTacheRetard($week);
		$recolteRetard = $app['model']->getRecoltePrevTacheRetard($week);

		$tabSemisRetard = array();
		$i = 0;
		foreach($semisRetard as $prev) {
			//récupération du code légume + code emplacement
			$tabSemisRetard[$i]['code'] = $app['model']->getCode($prev['idEmplacementReserve']);
			$tabSemisRetard[$i]['prevision'] = $prev;
			$i++;
		}

		$tabPlantationRetard = array();
		$i = 0;
		foreach($plantationRetard as $prev) {
			//récupération du code légume + code emplacement
			$tabPlantationRetard[$i]['code'] = $app['model']->getCode($prev['idEmplacementReserve']);
			$tabPlantationRetard[$i]['prevision'] = $prev;
			$i++;
		}

		$tabRecolteRetard = array();
		$i = 0;
		foreach($recolteRetard as $prev) {
			//récupération du code légume + code emplacement
			$tabRecolteRetard[$i]['code'] = $app['model']->getCode($prev['idEmplacementReserve']);
			$tabRecolteRetard[$i]['prevision'] = $prev;
			$i++;
		}
		
		array_push($taches['retard'], $tabSemisRetard);
		array_push($taches['retard'], $tabPlantationRetard);
		array_push($taches['retard'], $tabRecolteRetard);

		return $app['twig']->render('taches.html.twig', array(
			"annee" => $year,
			"semaine" => $weekNumber,
			"taches" => $taches
		));
	})->bind('taches');

	$app->match('/taches/{annee}-{semaine}', function($annee, $semaine) use ($app) {
		$week = array();
		$today = date("Y-m-d");
		$date = new DateTime($today);
		$actualWeek = $date->format("W");
		$year = date("Y");
		$week = array();

		if(isset($semaine) && isset($annee)) {
			//on recupere et modifie l'année
			$aux1 = $year - $annee;
			if($aux1 > 0)
				$date->modify("-".$aux1." years");
			else if($aux1 < 0)
				$date->modify("+".abs($aux1)." years");
			//on recupere et modifie la semaine
			$aux2 = $actualWeek - $semaine;
			if($aux2 > 0)
				$date->modify("-".$aux2." weeks");
			else if($aux2 < 0)
				$date->modify("+".abs($aux2)." weeks");
			//on calcule nos jours
			$weekNumber = $date->format("W");
			$yearNumber = $date->format("Y");
			for($day=1; $day<=7; $day++)
			{
			    $week[] = date('Y-m-d', strtotime($yearNumber."W".$weekNumber.$day));
			}
		}
		else {
			$weekNumber = $date->format("W");
			
			for($day=1; $day<=7; $day++)
			{
			    $week[] = date('Y-m-d', strtotime($year."W".$weekNumber.$day));
			}
		}

		$taches = array();

		$taches['previsionnel'] = array();
		$taches['retard'] = array();
		$taches['suivi'] = array();

		//previsions et suivis
		$semisPrev = $app['model']->getSemisPrevisionnelTache($week);
		$plantationPrev = $app['model']->getPlantationPrevisionnelTache($week);
		$recoltePrev = $app['model']->getRecoltePrevisionnelTache($week);

		$tabSemisPrev = array();
		$tabSemisSuivi = array();
		$i = 0;
		foreach($semisPrev as $prev) {
			$semisSuivi = $app['model']->getSuiviByPrevisionnel($prev['idPrevisionnel']);
			if($semisSuivi['semis'] != null) {
				//récupération du code légume + code emplacement
				$tabSemisSuivi[$i]['code'] = $app['model']->getCode($prev['idEmplacementReserve']);
				$tabSemisSuivi[$i]['prevision'] = $semisSuivi;
			}
			else {
				//récupération du code légume + code emplacement
				$tabSemisPrev[$i]['code'] = $app['model']->getCode($prev['idEmplacementReserve']);
				$tabSemisPrev[$i]['prevision'] = $prev;
			}

			$i++;
		}

		$tabPlantationPrev = array();
		$tabPlantationSuivi = array();
		$i = 0;
		foreach($plantationPrev as $prev) {
			$plantationSuivi = $app['model']->getSuiviByPrevisionnel($prev['idPrevisionnel']);
			if($plantationSuivi['plantation'] != null) {
				//récupération du code légume + code emplacement
				$tabPlantationSuivi[$i]['code'] = $app['model']->getCode($prev['idEmplacementReserve']);
				$tabPlantationSuivi[$i]['prevision'] = $plantationSuivi;
			}
			else {
				//récupération du code légume + code emplacement
				$tabPlantationPrev[$i]['code'] = $app['model']->getCode($prev['idEmplacementReserve']);
				$tabPlantationPrev[$i]['prevision'] = $prev;
			}

			$i++;
		}

		$tabRecoltePrev = array();
		$tabRecolteSuivi = array();
		$i = 0;
		foreach($recoltePrev as $prev) {
			$recolteSuivi = $app['model']->getSuiviByPrevisionnel($prev['idPrevisionnel']);
			if($recolteSuivi['debutRecolte'] != null || $recolteSuivi['finRecolte'] != null) {
				//récupération du code légume + code emplacement
				$tabRecolteSuivi[$i]['code'] = $app['model']->getCode($prev['idEmplacementReserve']);
				$tabRecolteSuivi[$i]['prevision'] = $recolteSuivi;
			}
			else {
				//récupération du code légume + code emplacement
				$tabRecoltePrev[$i]['code'] = $app['model']->getCode($prev['idEmplacementReserve']);
				$tabRecoltePrev[$i]['prevision'] = $prev;
			}

			$i++;
		}

		array_push($taches['previsionnel'], $tabSemisPrev);
		array_push($taches['previsionnel'], $tabPlantationPrev);
		array_push($taches['previsionnel'], $tabRecoltePrev);

		array_push($taches['suivi'], $tabSemisSuivi);
		array_push($taches['suivi'], $tabPlantationSuivi);
		array_push($taches['suivi'], $tabRecolteSuivi);


		//previsions en retard
		$semisRetard = $app['model']->getSemisPrevTacheRetard($week);
		$plantationRetard = $app['model']->getPlantationPrevTacheRetard($week);
		$recolteRetard = $app['model']->getRecoltePrevTacheRetard($week);

		$tabSemisRetard = array();
		$i = 0;
		foreach($semisRetard as $prev) {
			//récupération du code légume + code emplacement
			$tabSemisRetard[$i]['code'] = $app['model']->getCode($prev['idEmplacementReserve']);
			$tabSemisRetard[$i]['prevision'] = $prev;
			$i++;
		}

		$tabPlantationRetard = array();
		$i = 0;
		foreach($plantationRetard as $prev) {
			//récupération du code légume + code emplacement
			$tabPlantationRetard[$i]['code'] = $app['model']->getCode($prev['idEmplacementReserve']);
			$tabPlantationRetard[$i]['prevision'] = $prev;
			$i++;
		}

		$tabRecolteRetard = array();
		$i = 0;
		foreach($recolteRetard as $prev) {
			//récupération du code légume + code emplacement
			$tabRecolteRetard[$i]['code'] = $app['model']->getCode($prev['idEmplacementReserve']);
			$tabRecolteRetard[$i]['prevision'] = $prev;
			$i++;
		}
	
		array_push($taches['retard'], $tabSemisRetard);
		array_push($taches['retard'], $tabPlantationRetard);
		array_push($taches['retard'], $tabRecolteRetard);

		return $app['twig']->render('taches.html.twig', array(
			"annee" => $year,
			"semaine" => $weekNumber,
			"taches" => $taches
		));
	})->bind('semaineTache');

	$app->post('/taches/infoPopover', function() use ($app) {
		if(!empty($_POST['idPrev'])) {
			$tab = $app['model']->getLegumeVarieteCommentaireByPrev($_POST['idPrev']);
			return $app->json($tab);
		}
	})->bind('popoverTache');

	$app->post('/taches/layoutPopover', function() use ($app) {
		$today = date("Y-m-d");
		$date = new DateTime($today);
		$weekNumber = $date->format("W");
		$year = date("Y");
		$week = array();
		for($day=1; $day<=7; $day++)
		{
		    $week[] = date('Y-m-d', strtotime($year."W".$weekNumber.$day));
		}

		$popover = array();

		//we get previsionnal tasks and retard tasks
		$semisPrev = $app['model']->getSemisPrevisionnelTache($week);
		$plantationPrev = $app['model']->getPlantationPrevisionnelTache($week);
		$recoltePrev = $app['model']->getRecoltePrevisionnelTache($week);
		$semisRetard = $app['model']->getSemisPrevTacheRetard($week);
		$plantationRetard = $app['model']->getPlantationPrevTacheRetard($week);
		$recolteRetard = $app['model']->getRecoltePrevTacheRetard($week);

		$tacheprev = 0;
		$tacheretard = 0;
		//we count nb of previsionnal tasks and retard tasks
		foreach($semisPrev as $prev)
			$tacheprev++;
		foreach($plantationPrev as $prev)
			$tacheprev++;
		foreach($recoltePrev as $prev)
			$tacheprev++;
		foreach($semisRetard as $prev) {
			$tacheretard++;
			$tacheprev++;
		}
		foreach($plantationRetard as $prev) {
			$tacheretard++;
			$tacheprev++;
		}
		foreach($recolteRetard as $prev) {
			$tacheretard++;
			$tacheprev++;
		}

		array_push($popover, $tacheprev);
		array_push($popover, $tacheretard);
		//return the count
		return $app->json($popover);
	})->bind('popoverLayout');

	$app->post('/taches/save/{type}', function($type) use ($app) {
		var_dump($type);
		if($type == 0) { //if the user save preparation sol tasks
			if(isset($_POST['sol']) && isset($_POST['idPrev']) && isset($_POST['idReserve'])) {

				for($i = 0; $i < count($_POST['sol']); $i++){
					//get the suivi by the previsionnal and check if it exists
					$suivi = $app['model']->getSuiviByPrevisionnel($_POST['idPrev'][$i]);

					$sol = date("Y-m-d", strtotime(str_replace('/', '-', $_POST['sol'][$i])));
					if($suivi == null ) {
						var_dump($sol);
						var_dump($_POST['idPrev'][$i]);
						var_dump($_POST['idReserve'][$i]);
						$app['model']->setSuivi($sol, null, null, null, null, null, null, null, null, null, $_POST['idPrev'][$i], $_POST['idReserve'][$i]);
					}
					else {
						$tabSuivi = array();
						for($j = 1; $j <= 10; $j++) {
							// we look if the field in the suivi is a date or not, null or not
							if($suivi[$j] == null)
								array_push($tabSuivi, null);
							else if($suivi[$j] != null && date_parse($suivi[$j]) != false)
								array_push($tabSuivi, date("Y-m-d", strtotime($suivi[$j])));
							else array_push($tabSuivi, $suivi[$j]);
						}

						$app['model']->updateSuivi($sol, $tabSuivi[1], $tabSuivi[2], $tabSuivi[3], $tabSuivi[4], $tabSuivi[5], $tabSuivi[6], $tabSuivi[7], $tabSuivi[8], $tabSuivi[9], $suivi['idSuivi']);
					}
				}
			}

		}
		else if($type == 1) { //if the user save lutte adventices tasks
			if(isset($_POST['adventices']) && isset($_POST['idPrev']) && isset($_POST['idReserve'])) {

				for($i = 0; $i < count($_POST['adventices']); $i++){
					//get the suivi by the previsionnal and check if it exists
					$suivi = $app['model']->getSuiviByPrevisionnel($_POST['idPrev'][$i]);

					$adventices = date("Y-m-d", strtotime(str_replace('/', '-', $_POST['adventices'][$i])));
					if($suivi == null ) {
						$app['model']->setSuivi(null, $adventices, null, null, null, null, null, null, null, null, $_POST['idPrev'][$i], $_POST['idReserve'][$i]);
					}
					else {
						$tabSuivi = array();
						for($j = 1; $j <= 10; $j++) {
							// we look if the field in the suivi is a date or not, null or not
							if($suivi[$j] == null)
								array_push($tabSuivi, null);
							else if($suivi[$j] != null && date_parse($suivi[$j]) != false)
								array_push($tabSuivi, date("Y-m-d", strtotime($suivi[$j])));
							else array_push($tabSuivi, $suivi[$j]);
						}

						$app['model']->updateSuivi($tabSuivi[0], $adventices, $tabSuivi[2], $tabSuivi[3], $tabSuivi[4], $tabSuivi[5], $tabSuivi[6], $tabSuivi[7], $tabSuivi[8], $tabSuivi[9], $suivi['idSuivi']);
					}
				}
			}
		}
		else if($type == 2) { //if the user save semis tasks
			if(isset($_POST['semis']) && isset($_POST['idPrev']) && isset($_POST['idReserve'])) {

				for($i = 0; $i < count($_POST['adventices']); $i++){
					//get the suivi by the previsionnal and check if it exists
					$suivi = $app['model']->getSuiviByPrevisionnel($_POST['idPrev'][$i]);

					$semis = date("Y-m-d", strtotime(str_replace('/', '-', $_POST['semis'][$i])));
					if($suivi == null ) {
						$app['model']->setSuivi(null, null, $semis, null, null, null, null, null, null, null, $_POST['idPrev'][$i], $_POST['idReserve'][$i]);
					}
					else {
						$tabSuivi = array();
						for($j = 1; $j <= 10; $j++) {
							// we look if the field in the suivi is a date or not, null or not
							if($suivi[$j] == null)
								array_push($tabSuivi, null);
							else if($suivi[$j] != null && date_parse($suivi[$j]) != false)
								array_push($tabSuivi, date("Y-m-d", strtotime($suivi[$j])));
							else array_push($tabSuivi, $suivi[$j]);
						}

						$app['model']->updateSuivi($tabSuivi[0], $tabSuivi[1], $semis, $tabSuivi[3], $tabSuivi[4], $tabSuivi[5], $tabSuivi[6], $tabSuivi[7], $tabSuivi[8], $tabSuivi[9], $suivi['idSuivi']);
					}
				}
			}
		}
		else if($type == 3) { //if the user save plantation tasks
			if(isset($_POST['plantation']) && isset($_POST['idPrev']) && isset($_POST['idReserve'])) {

				for($i = 0; $i < count($_POST['plantation']); $i++){
					if(!empty($_POST['plantation'][$i])) {
						//get the suivi by the previsionnal and check if it exists
						$suivi = $app['model']->getSuiviByPrevisionnel($_POST['idPrev'][$i]);

						$plantation = date("Y-m-d", strtotime(str_replace('/', '-', $_POST['plantation'][$i])));
						if($suivi == null ) {
							$app['model']->setSuivi(null, null, null, $plantation, null, null, null, null, null, null, $_POST['idPrev'][$i], $_POST['idReserve'][$i]);
						}
						else {
							$tabSuivi = array();
							for($j = 1; $j <= 10; $j++) {
								// we look if the field in the suivi is a date or not, null or not
								if($suivi[$j] == null)
									array_push($tabSuivi, null);
								else if($suivi[$j] != null && date_parse($suivi[$j]) != false)
									array_push($tabSuivi, date("Y-m-d", strtotime($suivi[$j])));
								else array_push($tabSuivi, $suivi[$j]);
							}

							$app['model']->updateSuivi($tabSuivi[0], $tabSuivi[1], $tabSuivi[2], $plantation, $tabSuivi[4], $tabSuivi[5], $tabSuivi[6], $tabSuivi[7], $tabSuivi[8], $tabSuivi[9], $suivi['idSuivi']);
						}
					}
				}
			}
		}
		else if($type == 4) { //if the user save recolte tasks
			if((isset($_POST['recolteDebut']) || isset($_POST['recolteFin'])) && isset($_POST['idPrev']) && isset($_POST['idReserve'])) {

				for($i = 0; $i < count($_POST['recolteDebut']); $i++){
					//get the suivi by the previsionnal and check if it exists
					$suivi = $app['model']->getSuiviByPrevisionnel($_POST['idPrev'][$i]);

					$recolteDebut = date("Y-m-d", strtotime(str_replace('/', '-', $_POST['recolteDebut'][$i])));
					if($suivi == null ) {
						$app['model']->setSuivi(null, null, null, null, $recolteDebut, null, null, null, null, null, $_POST['idPrev'][$i], $_POST['idReserve'][$i]);
					}
					else {
						$tabSuivi = array();
						for($j = 1; $j <= 10; $j++) {
							// we look if the field in the suivi is a date or not, null or not
							if($suivi[$j] == null)
								array_push($tabSuivi, null);
							else if($suivi[$j] != null && date_parse($suivi[$j]) != false)
								array_push($tabSuivi, date("Y-m-d", strtotime($suivi[$j])));
							else array_push($tabSuivi, $suivi[$j]);
						}

						$app['model']->updateSuivi($tabSuivi[0], $tabSuivi[1], $tabSuivi[2], $tabSuivi[3], $recolteDebut, $tabSuivi[5], $tabSuivi[6], $tabSuivi[7], $tabSuivi[8], $tabSuivi[9], $suivi['idSuivi']);
					}
				}

				for($i = 0; $i < count($_POST['recolteFin']); $i++){
					//get the suivi by the previsionnal and check if it exists
					$suivi = $app['model']->getSuiviByPrevisionnel($_POST['idPrev'][$i]);

					$recolteFin = date("Y-m-d", strtotime(str_replace('/', '-', $_POST['recolteFin'][$i])));
					if($suivi == null ) {
						$app['model']->setSuivi(null, null, null, null, null, $recolteFin, null, null, null, null, $_POST['idPrev'][$i], $_POST['idReserve'][$i]);
					}
					else {
						$tabSuivi = array();
						for($j = 1; $j <= 10; $j++) {
							// we look if the field in the suivi is a date or not, null or not
							if($suivi[$j] == null)
								array_push($tabSuivi, null);
							else if($suivi[$j] != null && date_parse($suivi[$j]) != false)
								array_push($tabSuivi, date("Y-m-d", strtotime($suivi[$j])));
							else array_push($tabSuivi, $suivi[$j]);
						}

						$app['model']->updateSuivi($tabSuivi[0], $tabSuivi[1], $tabSuivi[2], $tabSuivi[3], $tabSuivi[4], $recolteFin, $tabSuivi[6], $tabSuivi[7], $tabSuivi[8], $tabSuivi[9], $suivi['idSuivi']);
					}
				}
			}
		}

		return $app->redirect('/taches');
	})->bind('saveTache');

	//
	/////////DONNEES/////////
	//
	$app->match('/data', function() use ($app) {
		return $app['twig']->render('datas.html.twig', array(
			'legumes' => $app['model']->getAllLegume(),
			'varietes' => $app['model']->getAllVarietes(),
			'semenciers' => $app['model']->getAllSemenciers(),
			'emplacements' => $app['model']->getAllEmplacement(),
			'previsions' => $app['model']->getAllPrevisions(),
			'suivis' => $app['model']->getAllSuivis()
		));
	})->bind('data');

	$app->post('/data/export', function(Request $request) use ($app) {
		if($request->get('nomExportFichier') != null) {
			$filename = $request->get('nomExportFichier');
			$basePath = __DIR__.'\\doc\\telechargements';
	        $filePath = $basePath.'\\'.$filename.'.sql';

	        // check if file exists
	        if (file_exists($filePath)) {
	            return $app['twig']->render('datas.html.twig', array(
	            	'resultDataExport' => false
	            ));
	        }
	        else {
	        	$back = new Classe\BackupMySQL($filePath, array(
					'username' => 'root',
					'passwd' => '',
					'dbname' => 'seed'
				));

     			$back->exportSql();
        		return $app['twig']->render('datas.html.twig', array(
        			'resultDataExport' => "true"
       			));
	        }	    
	    }
	    else
	    {
	    	return $app['twig']->render('datas.html.twig', array(
	            	'resultDataExport' => "false"
	        ));
	    }

	})->bind('exportData');

	$app->post('/data/import', function(Request $request) use ($app) {
		if($request->get('nomImportFichier') != null) {
			$filename = $request->get('nomImportFichier');
			$basePath = __DIR__.'\\doc\\telechargements';
	        $filePath = $basePath.'\\'.$filename;

			$back = new Classe\BackupMySQL($filePath, array(
				'username' => 'root',
				'passwd' => '',
				'dbname' => 'seed'
			));

			$tab = explode('.', $request->get('nomImportFichier'));
			if($tab[1] == "sql") {
				//.sql
				$back->importSql();

				return $app['twig']->render('datas.html.twig', array(
        			'resultDataImport' => "true"
       			));
			}
			else {
				return $app['twig']->render('datas.html.twig', array(
        			'resultDataImport' => "false"
       			));
			}
		}
	})->bind('importData');

	//
	/////////PLANNING/////////
	//
	$app->match("/planning", function() use ($app){
		return $app['twig']->render("planning.html.twig", array(
			"emplacementsR" => $app['model']->getAllEmplacementReserve(),
			"emplacements" => $app['model']->getAllEmplacement(),
			"legumes" => $app['model']->getAllLegume()
		));
	})->bind("planning");

	$app->match("/planning/get", function() use ($app){
		return $app['twig']->render("planning.html.twig", array(
			"emplacementsR" => $app['model']->getAllEmplacementReserve(),
			"emplacements" => $app['model']->getAllEmplacement(),
			"legumes" => $app['model']->getAllLegume()
		));
	})->bind("planning");
	
	$app->post("/planning/add", function() use ($app){
		if(!empty($_POST['idEmplacement']) && !empty($_POST['idVariete'])) {
			if($app['model']->setEmplacementReserve($_POST['idEmplacement'],$_POST['idVariete']) == "success") {
				return $app['twig']->render("planning.html.twig", array(
					"emplacementsR" => $app['model']->getAllEmplacementReserve(),
					"emplacements" => $app['model']->getAllEmplacement(),
					"legumes" => $app['model']->getAllLegume(),
					"result" => "true"
				));
			}
		}
		return $app['twig']->render("planning.html.twig", array(
			"emplacementsR" => $app['model']->getAllEmplacementReserve(),
			"emplacements" => $app['model']->getAllEmplacement(),
			"legumes" => $app['model']->getAllLegume(),
			"result" => "false"
		));
	})->bind("addPlanning");

	$app->post("/planning/delete", function() use ($app){
		if(!empty($_POST['idEmplacementReserve'])) {
			if($app['model']->deleteEmplacementReserve($_POST['idEmplacementReserve'])) {
				return $app['twig']->render("planning.html.twig", array(
					"emplacementsR" => $app['model']->getAllEmplacementReserve(),
					"emplacements" => $app['model']->getAllEmplacement(),
					"legumes" => $app['model']->getAllLegume(),
					"delete" => "true"
				));
			}
		}
		return $app['twig']->render("planning.html.twig", array(
			"emplacementsR" => $app['model']->getAllEmplacementReserve(),
			"emplacements" => $app['model']->getAllEmplacement(),
			"legumes" => $app['model']->getAllLegume(),
			"delete" => "false"
		));
	})->bind("deletePlanning");
	
	//gestion du gantt général dans le planning
	$app->post("/planning/get/getGantt", function() use ($app) {
		
			return json_encode($app['model']->getAllPrevisionnelAndVariete());
		
		
	});

	//
	/////////GESTION LEGUME/////////
	//
	$app->match('/legume', function() use ($app){
		return $app['twig']->render('legume.html.twig', array(
			'legumes' => $app['model']->getAllLegume(),
			'varietes' => $app['model']->getAllVarietes(),
			'semenciers' => $app['model']->getAllSemenciers()
		));
	})->bind('legume');

	$app->post('/legume/filter', function() use ($app) {
		$idLegume = $_POST['idLegume'];                       
		if(!is_numeric($idLegume)) {
			$idLegume = null;
		}
		$varietes = $app['model']->getAllVarietes($idLegume);
		return json_encode($varietes);
	})->bind("legumeFilter");

	/*ajouter légume */
	$app->post('/legume/add', function() use ($app) {
		if(!empty($_POST['nomLegume'])) {
			if($app['model']->addLegume($_POST['nomLegume']) == "success") {
				return $app['twig']->render('legume.html.twig', array(
					'legumes' => $app['model']->getAllLegume(),
					'varietes' => $app['model']->getAllVarietes(),
					'semenciers' => $app['model']->getAllSemenciers(),
					'var' => $_POST['nomLegume'],
					'resultLegume' => "true"
				));
			} else {
				return $app['twig']->render('legume.html.twig', array(
					'legumes' => $app['model']->getAllLegume(),
					'varietes' => $app['model']->getAllVarietes(),
					'semenciers' => $app['model']->getAllSemenciers(),
					'var' => $_POST['nomLegume'],
					'resultLegume' => "false"
				));
			}
		}
	})->bind('addLegume');

	/* Recherche d'un légume en particuler */
	$app->post("/legume/get", function() use ($app){
		if(!empty($_POST['idLegume']))
			return json_encode($app['model']->getLegume($_POST['idLegume']));
	});
	
	/* mise à jour légume */
	$app->post('/legume/update', function() use ($app) {
		if(!empty($_POST['nomLegume'])) {
			if($app['model']->updateLegume($_POST['idLegume'],$_POST['nomLegume']) == "success") {
				return $app['twig']->render('legume.html.twig', array(
					'legumes' => $app['model']->getAllLegume(),
					'varietes' => $app['model']->getAllVarietes(),
					'semenciers' => $app['model']->getAllSemenciers(),
					'var' => $_POST['nomLegume'],
					'updateLegume' => "true"
				));
			}
		}
		return $app['twig']->render('legume.html.twig', array(
			'legumes' => $app['model']->getAllLegume(),
			'varietes' => $app['model']->getAllVarietes(),
			'semenciers' => $app['model']->getAllSemenciers(),
			'var' => $_POST['nomLegume'],
			'updateLegume' => "false"
		));
	})->bind('updateLegume');
	
	/* delete legume */
	$app->post('/legume/delete', function() use ($app) {
		$nameleg = $app['model']->getLegume($_POST['idLegume']);
		if(!empty($_POST['idLegume'])) {
			if($app['model']->deleteLegume($_POST['idLegume']) == "success") {
				return $app['twig']->render('legume.html.twig', array(
					'legumes' => $app['model']->getAllLegume(),
					'varietes' => $app['model']->getAllVarietes(),
					'semenciers' => $app['model']->getAllSemenciers(),
					'var' => $nameleg['nom'],
					'deleteLegume' => "true"
				));
			}
		}
		return $app['twig']->render('legume.html.twig', array(
			'legumes' => $app['model']->getAllLegume(),
			'varietes' => $app['model']->getAllVarietes(),
			'semenciers' => $app['model']->getAllSemenciers(),
			'var' => $nameleg['nom'],
			'deleteLegume' => "false"
		));
	})->bind('deleteLegume');
	
	//
	/////////GESTION VARIETE/////////
	//
	$app->post('/legume/variete/add', function() use ($app){
		if(!empty($_POST['idLegume']) && !empty($_POST['nom']) && !empty($_POST['codeLegume']) && !empty($_POST['idSemencier'])){
			if($app['model']->setVariete($_POST['nom'], $_POST['codeLegume'], $_POST['commentaire'], $_POST['idLegume'], $_POST['idSemencier']) == "success") {
				return $app['twig']->render('legume.html.twig', array(
					'legumes' => $app['model']->getAllLegume(),
					'varietes' => $app['model']->getAllVarietes(),
					'semenciers' => $app['model']->getAllSemenciers(),
					'var' => $_POST['nom'],
					'resultVariete' => "true"
				));
			}
		}
		return $app['twig']->render('legume.html.twig', array(
			'legumes' => $app['model']->getAllLegume(),
			'varietes' => $app['model']->getAllVarietes(),
			'semenciers' => $app['model']->getAllSemenciers(),
			'var' => $_POST['nom'],
			'resultVariete' => "false"
		));
	})->bind('addVariete');

	/* Recherche d'une variété particulière */
	$app->post("/legume/variete/get", function() use ($app) {
		if(!empty($_POST['idVariete']))
			return json_encode($app['model']->getVariete($_POST['idVariete']));
	});
	
	/* Modification d'une variété */
	$app->post('/legume/variete/update', function() use ($app) {
		if(!empty($_POST['idLegume']) && !empty($_POST['nom']) && !empty($_POST['codeLegume']) && !empty($_POST['idSemencier'])) {
			if($app['model']->updateVariete($_POST['nom'],$_POST['codeLegume'],$_POST['commentaire'],$_POST['idLegume'],$_POST['idSemencier'],$_POST['idVariete'])) {
				return $app['twig']->render('legume.html.twig', array(
					'legumes' => $app['model']->getAllLegume(),
					'varietes' => $app['model']->getAllVarietes(),
					'semenciers' => $app['model']->getAllSemenciers(),
					'var' => $_POST['nom'],
					'updateVariete' => "true"
				));
			}
		}
		return $app['twig']->render('legume.html.twig', array(
			'legumes' => $app['model']->getAllLegume(),
			'varietes' => $app['model']->getAllVarietes(),
			'semenciers' => $app['model']->getAllSemenciers(),
			'var' => $_POST['nomLegume'],
			'updateVariete' => "false"
		));
	})->bind('updateVariete');

	/* Suppression d'une variété */
	$app->post("/legume/variete/delete", function() use ($app) {
		if(!empty($_POST['idVariete'])) {
			if($app['model']->deleteVariete($_POST['idVariete'])) {
				return $app['twig']->render('legume.html.twig', array(
					'legumes' => $app['model']->getAllLegume(),
					'varietes' => $app['model']->getAllVarietes(),
					'semenciers' => $app['model']->getAllSemenciers(),
					'deleteVariete' => "true"
				));
			}
		}
		return $app['twig']->render('legume.html.twig', array(
			'legumes' => $app['model']->getAllLegume(),
			'varietes' => $app['model']->getAllVarietes(),
			'semenciers' => $app['model']->getAllSemenciers(),
			'deleteVariete' => "false"
		));

	})->bind("deleteVariete");
	

	//
	/////////GESTION DES SEMENCIERS/////////
	//

	$app->match('/semencier', function() use ($app) {
		return $app['twig']->render("semencier.html.twig", array(
			"semenciers" => $app['model']->getAllSemenciers()
		));
	})->bind("semencier");

	$app->post('/semencier/add', function() use ($app) {
		if(!empty($_POST['nomSemencier'])) {
			if( $app['model']->setSemencier($_POST['nomSemencier']) == "success"){
					return $app['twig']->render("semencier.html.twig", array(
					'semenciers' => $app['model']->getAllSemenciers(),
					"var" => $_POST['nomSemencier'],
					'result' => 'true'
				));
			}
		}else{
			return $app['twig']->render("semencier.html.twig", array(
					'semenciers' => $app['model']->getAllSemenciers(),
					"var" => $_POST['nomSemencier'],
					'result' => 'false'
					));
		}
	})->bind("addSemencier");

	$app->post("/semencier/get", function() use ($app) {
		if(!empty($_POST['idSemencier'])) {
			return json_encode($app['model']->getSemencier($_POST['idSemencier']));
		}
	});
	
	$app->post("/semencier/getAll", function() use ($app) {
			return json_encode($app['model']->getAllSemenciers());
	});
	
	$app->post('/semencier/update', function() use ($app) {
		$nameSem = $app['model']->getSemencier($_POST['idSemencier']);
		if(!empty($_POST['idSemencier'])) {
			if($app['model']->updateSemencier($_POST['idSemencier'],$_POST['nomSemencier']) == "success") {
				return $app['twig']->render('semencier.html.twig', array(
					'semenciers' => $app['model']->getAllSemenciers(),
					'updateSemencier' => "true",
					'var' => $_POST['nomSemencier']
				));
			}
		}
	})->bind("updateSemencier");
	
	$app->post('/semencier/delete', function() use ($app) {
		$var = $app['model']->getSemencier($_POST['idSemencier']);
		if(!empty($_POST['idSemencier'])) {
			if($app['model']->deleteSemencier($_POST['idSemencier']) == "success") {
				return $app['twig']->render('semencier.html.twig', array(
					'semenciers' => $app['model']->getAllSemenciers(),
					'deleteSemencier' => "true",
					'var' => $var['nom']
				));
			}
		}
		return $app['twig']->render('semencier.html.twig', array(
			'semenciers' => $app['model']->getAllSemenciers(),
			'deleteSemencier' => "false",
			'var' => $var['nom']
		));
	})->bind("deleteSemencier");
	
	//
	/////////GESTIONS DES EMPLACEMENTS/////////
	//

	$app->match('/emplacement', function() use ($app){
		return $app['twig']->render("emplacement.html.twig", array(
			"emplacements" => $app['model']->getAllEmplacement()
		));
	})->bind("emplacement");

	$app->post("/emplacement/add", function() use ($app){
		if(!empty($_POST['parcelle']) && !empty($_POST['carreCulture']) && !empty($_POST['planche']) && !empty($_POST['codeEmplacement'])) {
			if($app['model']->setEmplacement($_POST['parcelle'],$_POST['carreCulture'],$_POST['planche'],$_POST['codeEmplacement'],$_POST['commentaire'])) {
				return $app['twig']->render("emplacement.html.twig", array(
					"result" => "true",
					"emplacements" => $app['model']->getAllEmplacement()
				));
			}
		}
		return $app['twig']->render("emplacement.html.twig", array(
			"result" => "false",
			"emplacements" => $app['model']->getAllEmplacement()
		));
	})->bind("addEmplacement");

	$app->post("/emplacement/get", function() use ($app) {
		if(!empty($_POST['idEmplacement']))
			return json_encode($app['model']->getEmplacement($_POST['idEmplacement']));
	});

	$app->post("/emplacement/edit", function() use ($app) {
		if(!empty($_POST['parcelle']) && !empty($_POST['carreCulture']) && !empty($_POST['planche']) && !empty($_POST['codeEmplacement']) && !empty($_POST['idEmplacement'])) {
			if($app['model']->updateEmplacement($_POST['parcelle'],$_POST['carreCulture'],$_POST['planche'],$_POST['codeEmplacement'],$_POST['commentaire'],$_POST['idEmplacement'])) {
				return $app['twig']->render("emplacement.html.twig", array(
					"update" => "true",
					"emplacements" => $app['model']->getAllEmplacement()
				));
			}
		}
		return $app['twig']->render("emplacement.html.twig", array(
			"update" => "false",
			"emplacements" => $app['model']->getAllEmplacement()
		));
	})->bind("emplacementEdit");

	$app->post("/emplacement/delete", function() use ($app){
		if(!empty($_POST['idEmplacement'])) {
			if($app['model']->deleteEmplacement($_POST['idEmplacement'])) {
				return $app['twig']->render("emplacement.html.twig", array(
					"delete" => "true",
					"emplacements" => $app['model']->getAllEmplacement()
				));
			}
		}
		return $app['twig']->render("emplacement.html.twig", array(
			"delete" => "false",
			"emplacements" => $app['model']->getAllEmplacement()
		));
	})->bind("deleteEmplacement");

	//
	/////////GESTIONS DES PREVISIONS/////////
	//
	$app->get("/planning/prevision/{idEmplacementR}", function($idEmplacementR) use ($app){
		return $app['twig']->render("prevision.html.twig", array(
			"idEmplacementReserve" => $idEmplacementR,
			"emplacementReserve" => $app['model']->getEmplacementReserve($idEmplacementR),
			"previsions" => $app['model']->getAllPrevisionnel($idEmplacementR)
		));
	})->bind("prevision");

	$app->post("/planning/prevision/add", function() use ($app) {
		if(!empty($_POST['preparationSol']) && !empty($_POST['lutteAdventices']) && !empty($_POST['semis']) && !empty($_POST['plantation']) && !empty($_POST['debutRecolte']) && !empty($_POST['finRecolte']) && !empty($_POST['idEmplacementReserve'])) {
			//Conversion des dates dans le bon format
			$semis = date("Y-m-d", strtotime(str_replace('/', '-', $_POST['semis'])));
			$plantation = date("Y-m-d", strtotime(str_replace('/', '-', $_POST['plantation'])));
			$debutRecolte = date("Y-m-d", strtotime(str_replace('/', '-', $_POST['debutRecolte'])));
			$finRecolte = date("Y-m-d", strtotime(str_replace('/', '-', $_POST['finRecolte'])));;

			if($app['model']->addPrevisionnel($_POST['preparationSol'],$_POST['lutteAdventices'],$semis,$plantation,$debutRecolte,$finRecolte,$_POST['idEmplacementReserve'])) {
				return $app['twig']->render("prevision.html.twig", array(
					"idEmplacementReserve" => $_POST['idEmplacementReserve'],
					"emplacementReserve" => $app['model']->getEmplacementReserve($_POST['idEmplacementReserve']),
					"previsions" => $app['model']->getAllPrevisionnel($_POST['idEmplacementReserve']),
					"result" => "true"
				));
			}
		}
		return $app['twig']->render("prevision.html.twig", array(
			"idEmplacementReserve" => $_POST['idEmplacementReserve'],
			"emplacementReserve" => $app['model']->getEmplacementReserve($_POST['idEmplacementReserve']),
			"previsions" => $app['model']->getAllPrevisionnel($_POST['idEmplacementReserve']),
			"result" => "false"
		));
	})->bind("addPrevision");

	$app->post("/planning/prevision/get", function() use ($app) {
		if(!empty($_POST['idPrevision']))
			return json_encode($app['model']->getPrevisionnel($_POST['idPrevision']));

		return 0;
	});

	$app->post("/planning/prevision/edit", function() use ($app) {
		if(!empty($_POST['preparationSol']) && !empty($_POST['lutteAdventices']) && !empty($_POST['semis']) && !empty($_POST['plantation']) && !empty($_POST['debutRecolte']) && !empty($_POST['finRecolte']) && !empty($_POST['idEmplacementReserve']) && !empty($_POST['idPrevisionnel'])) {
			//Conversion des dates dans le bon format
			$semis = date("Y-m-d", strtotime(str_replace('/', '-', $_POST['semis'])));
			$plantation = date("Y-m-d", strtotime(str_replace('/', '-', $_POST['plantation'])));
			$debutRecolte = date("Y-m-d", strtotime(str_replace('/', '-', $_POST['debutRecolte'])));
			$finRecolte = date("Y-m-d", strtotime(str_replace('/', '-', $_POST['finRecolte'])));
			if($app['model']->setPrevisionnel($_POST['preparationSol'],$_POST['lutteAdventices'],$semis,$plantation,$debutRecolte,$finRecolte,$_POST['idPrevisionnel'])) {
				return $app['twig']->render("prevision.html.twig", array(
					"idEmplacementReserve" => $_POST['idEmplacementReserve'],
					"emplacementReserve" => $app['model']->getEmplacementReserve($_POST['idEmplacementReserve']),
					"previsions" => $app['model']->getAllPrevisionnel($_POST['idEmplacementReserve']),
					"resultEdit" => "true"
				));
			}
		}
		return $app['twig']->render("prevision.html.twig", array(
			"idEmplacementReserve" => $_POST['idEmplacementReserve'],
			"emplacementReserve" => $app['model']->getEmplacementReserve($_POST['idEmplacementReserve']),
			"previsions" => $app['model']->getAllPrevisionnel($_POST['idEmplacementReserve']),
			"resultEdit" => "false"
		));
	})->bind("editPrevision");

	$app->post("/planning/prevision/delete", function() use ($app){
		if(!empty($_POST['idPrevisionnel'])) {
			if($app['model']->deletePrevisionnel($_POST['idPrevisionnel'])){
				return $app['twig']->render("prevision.html.twig", array(
					"idEmplacementReserve" => $_POST['idEmplacementReserve'],
					"emplacementReserve" => $app['model']->getEmplacementReserve($_POST['idEmplacementReserve']),
					"previsions" => $app['model']->getAllPrevisionnel($_POST['idEmplacementReserve']),
					"resultDelete" => "true"
				));
			}
		}
		return $app['twig']->render("prevision.html.twig", array(
			"idEmplacementReserve" => $_POST['idEmplacementReserve'],
			"emplacementReserve" => $app['model']->getEmplacementReserve($_POST['idEmplacementReserve']),
			"previsions" => $app['model']->getAllPrevisionnel($_POST['idEmplacementReserve']),
			"resultDelete" => "false"
		));
	})->bind('deletePrevision');


	//
	/////////GESTIONS DES SUIVIS/////////
	//
	$app->get("/planning/suivi/{idEmplacementR}", function($idEmplacementR) use ($app) {
		return $app['twig']->render("suivi.html.twig",array(
			"idEmplacementReserve" =>$idEmplacementR,
			"emplacementReserve" => $app['model']->getEmplacementReserve($idEmplacementR),
			"suivis" => $app['model']->getAllSuivi($idEmplacementR)
		));
	})->bind("suivi");

	$app->post("/planning/prevision/suivi/get", function() use ($app) {
		if(!empty($_POST['idPrevisionnel']) && !empty($_POST['idEmplacementReserve']))
			return json_encode($app['model']->getPrevisionnelSuivi($_POST['idPrevisionnel'],$_POST['idEmplacementReserve']));

		return 0;
	});

	$app->post("/planning/prevision/suivi/submit", function() use ($app) {
		if(isset($_POST['idPrevisionnel']) && isset($_POST['idEmplacementReserve'])) {
			$preparationSol = null;
			$lutteAdventices = null;
			$semis = null;
			$plantation = null;
			$debutRecolte = null;
			$finRecolte = null;

			if(!empty($_POST['preparationSol'])) 
				$preparationSol = date("Y-m-d", strtotime(str_replace('/', '-', $_POST['preparationSol'])));

			if(!empty($_POST['lutteAdventices']))
				$lutteAdventices = date("Y-m-d", strtotime(str_replace('/', '-', $_POST['lutteAdventices'])));

			if(!empty($_POST['semis']))
				$semis = date("Y-m-d", strtotime(str_replace('/', '-', $_POST['semis'])));

			if(!empty($_POST['plantation']))
				$plantation = date("Y-m-d", strtotime(str_replace('/', '-', $_POST['plantation'])));

			if(!empty($_POST['debutRecolte']))
				$debutRecolte = date("Y-m-d", strtotime(str_replace('/', '-', $_POST['debutRecolte'])));

			if(!empty($_POST['finRecolte']))
				$finRecolte = date("Y-m-d", strtotime(str_replace('/', '-', $_POST['finRecolte'])));

			//Ajout d'un suivi
			if(empty($_POST['idSuivi'])) {
				if($app['model']->setSuivi($preparationSol,$lutteAdventices, $semis, $plantation,$debutRecolte,$finRecolte,$_POST['quantite'],$_POST['unite'],$_POST['qualite'],$_POST['commentaire'],$_POST['idPrevisionnel'],$_POST['idEmplacementReserve'])) {
					return $app['twig']->render("prevision.html.twig", array(
						"idEmplacementReserve" => $_POST['idEmplacementReserve'],
						"emplacementReserve" => $app['model']->getEmplacementReserve($_POST['idEmplacementReserve']),
						"previsions" => $app['model']->getAllPrevisionnel($_POST['idEmplacementReserve']),
						"suivi" => "created"
					));
				}
			} else {
				//Modification d'un suivi
				if($app['model']->updateSuivi($preparationSol,$lutteAdventices, $semis, $plantation,$debutRecolte,$finRecolte,$_POST['quantite'],$_POST['unite'],$_POST['qualite'],$_POST['commentaire'],$_POST['idSuivi'])) {
					return $app['twig']->render("prevision.html.twig", array(
						"idEmplacementReserve" => $_POST['idEmplacementReserve'],
						"emplacementReserve" => $app['model']->getEmplacementReserve($_POST['idEmplacementReserve']),
						"previsions" => $app['model']->getAllPrevisionnel($_POST['idEmplacementReserve']),
						"suivi" => "edit"
					));
				}
			}
			return $app['twig']->render("prevision.html.twig", array(
				"idEmplacementReserve" => $_POST['idEmplacementReserve'],
				"emplacementReserve" => $app['model']->getEmplacementReserve($_POST['idEmplacementReserve']),
				"previsions" => $app['model']->getAllPrevisionnel($_POST['idEmplacementReserve']),
				"suivi" => "false"
			));
		}
	})->bind("submitSuivi");

	$app->post("/planning/suivi/delete", function() use ($app) {
		if(!empty($_POST['idSuivi'])) {
			if($app['model']->deleteSuivi($_POST['idSuivi'])) {
				return $app['twig']->render("suivi.html.twig",array(
					"idEmplacementReserve" => $_POST['idEmplacementReserve'],
					"emplacementReserve" => $app['model']->getEmplacementReserve($_POST['idEmplacementReserve']),
					"suivis" => $app['model']->getAllSuivi($_POST['idEmplacementReserve']),
					"delete" => "true"
				));
			}
		}
		return $app['twig']->render("suivi.html.twig",array(
			"idEmplacementReserve" => $_POST['idEmplacementReserve'],
			"emplacementReserve" => $app['model']->getEmplacementReserve($_POST['idEmplacementReserve']),
			"suivis" => $app['model']->getAllSuivi($_POST['idEmplacementReserve']),
			"delete" => "false"
		));
	})->bind("deleteSuivi");

	$app->post("/planning/suivi/add", function() use ($app) {
		if(!empty($_POST['idEmplacementReserve'])) {
			$preparationSol = null;
			$lutteAdventices = null;
			$semis = null;
			$plantation = null;
			$debutRecolte = null;
			$finRecolte = null;

			if(!empty($_POST['preparationSol'])) 
				$preparationSol = date("Y-m-d", strtotime(str_replace('/', '-', $_POST['preparationSol'])));

			if(!empty($_POST['lutteAdventices']))
				$lutteAdventices = date("Y-m-d", strtotime(str_replace('/', '-', $_POST['lutteAdventices'])));

			if(!empty($_POST['semis']))
				$semis = date("Y-m-d", strtotime(str_replace('/', '-', $_POST['semis'])));

			if(!empty($_POST['plantation']))
				$plantation = date("Y-m-d", strtotime(str_replace('/', '-', $_POST['plantation'])));

			if(!empty($_POST['debutRecolte']))
				$debutRecolte = date("Y-m-d", strtotime(str_replace('/', '-', $_POST['debutRecolte'])));

			if(!empty($_POST['finRecolte']))
				$finRecolte = date("Y-m-d", strtotime(str_replace('/', '-', $_POST['finRecolte'])));

			if($app['model']->setSuivi($preparationSol,$lutteAdventices, $semis, $plantation,$debutRecolte,$finRecolte,$_POST['quantite'],$_POST['unite'],$_POST['qualite'],$_POST['commentaire'],null,$_POST['idEmplacementReserve'])) {
				return $app['twig']->render("suivi.html.twig",array(
					"idEmplacementReserve" => $_POST['idEmplacementReserve'],
				 	"emplacementReserve" => $app['model']->getEmplacementReserve($_POST['idEmplacementReserve']),
				 	"suivis" => $app['model']->getAllSuivi($_POST['idEmplacementReserve']),
					"add" => "true"
				));
			}
		}
		return $app['twig']->render("suivi.html.twig",array(
			"idEmplacementReserve" => $_POST['idEmplacementReserve'],
			"emplacementReserve" => $app['model']->getEmplacementReserve($_POST['idEmplacementReserve']),
			"suivis" => $app['model']->getAllSuivi($_POST['idEmplacementReserve']),
			"add" => "false"
		));
	})->bind("addSuivi");

	$app->post("/planning/suivi/get", function() use ($app) {
		if(!empty($_POST['idSuivi'])) {	
			return json_encode($app['model']->getSuivi($_POST['idSuivi']));
		}

		return 0;
	});

	$app->post("/planning/suivi/edit", function() use ($app) {
		if(!empty($_POST['idSuivi'])) {
			$preparationSol = null;
			$lutteAdventices = null;
			$semis = null;
			$plantation = null;
			$debutRecolte = null;
			$finRecolte = null;

			if(!empty($_POST['preparationSol'])) 
				$preparationSol = date("Y-m-d", strtotime(str_replace('/', '-', $_POST['preparationSol'])));

			if(!empty($_POST['lutteAdventices']))
				$lutteAdventices = date("Y-m-d", strtotime(str_replace('/', '-', $_POST['lutteAdventices'])));

			if(!empty($_POST['semis']))
				$semis = date("Y-m-d", strtotime(str_replace('/', '-', $_POST['semis'])));

			if(!empty($_POST['plantation']))
				$plantation = date("Y-m-d", strtotime(str_replace('/', '-', $_POST['plantation'])));

			if(!empty($_POST['debutRecolte']))
				$debutRecolte = date("Y-m-d", strtotime(str_replace('/', '-', $_POST['debutRecolte'])));

			if(!empty($_POST['finRecolte']))
				$finRecolte = date("Y-m-d", strtotime(str_replace('/', '-', $_POST['finRecolte'])));

			if($app['model']->updateSuivi($preparationSol,$lutteAdventices, $semis, $plantation,$debutRecolte,$finRecolte,$_POST['quantite'],$_POST['unite'],$_POST['qualite'],$_POST['commentaire'],$_POST['idSuivi'])) {
				return $app['twig']->render("suivi.html.twig",array(
					"idEmplacementReserve" => $_POST['idEmplacementReserve'],
				 	"emplacementReserve" => $app['model']->getEmplacementReserve($_POST['idEmplacementReserve']),
				 	"suivis" => $app['model']->getAllSuivi($_POST['idEmplacementReserve']),
					"edit" => "true"
				));
			}
		}
		return $app['twig']->render("suivi.html.twig",array(
			"idEmplacementReserve" => $_POST['idEmplacementReserve'],
			"emplacementReserve" => $app['model']->getEmplacementReserve($_POST['idEmplacementReserve']),
			"suivis" => $app['model']->getAllSuivi($_POST['idEmplacementReserve']),
			"edit" => "false"
		));
	})->bind("editSuivi");


	// Fait remonter les erreurs
	$app->error(function($error) {
	    throw $error;
	    //$app->redirect('/planning');
	});


	$app->run();

?>