--
-- C:\wamp\www\Semences\semences\App'Seed\doc\telechargements\data.sql
SET FOREIGN_KEY_CHECKS=0;
CREATE DATABASE IF NOT EXISTS seed;
USE seed;


DROP TABLE IF EXISTS `emplacement`;
CREATE TABLE `emplacement` (
  `idEmplacement` int(11) NOT NULL AUTO_INCREMENT,
  `parcelle` varchar(45) NOT NULL,
  `carreCulture` varchar(45) NOT NULL,
  `planche` varchar(45) NOT NULL,
  `codeEmplacement` varchar(45) DEFAULT NULL,
  `commentaire` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`idEmplacement`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO `emplacement` VALUES ('1','Tunnel','1','1','T11','');
INSERT INTO `emplacement` VALUES ('2','Tunnel','1','2','T12','');
INSERT INTO `emplacement` VALUES ('3','Zone','3','9','Z39','');


DROP TABLE IF EXISTS `emplacementreserve`;
CREATE TABLE `emplacementreserve` (
  `idEmplacementReserve` int(11) NOT NULL AUTO_INCREMENT,
  `idEmplacement` int(11) NOT NULL,
  `idVariete` int(11) NOT NULL,
  PRIMARY KEY (`idEmplacementReserve`),
  KEY `fk_EmplacementReserve_Emplacement1_idx` (`idEmplacement`),
  KEY `fk_EmplacementReserve_Variete1_idx` (`idVariete`),
  CONSTRAINT `fk_EmplacementReserve_Emplacement1` FOREIGN KEY (`idEmplacement`) REFERENCES `emplacement` (`idEmplacement`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_EmplacementReserve_Variete1` FOREIGN KEY (`idVariete`) REFERENCES `variete` (`idVariete`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

INSERT INTO `emplacementreserve` VALUES ('1','1','1');


DROP TABLE IF EXISTS `legume`;
CREATE TABLE `legume` (
  `idLegume` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(45) NOT NULL,
  PRIMARY KEY (`idLegume`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO `legume` VALUES ('1','Tomate');
INSERT INTO `legume` VALUES ('2','Betterave');
INSERT INTO `legume` VALUES ('3','Salade');


DROP TABLE IF EXISTS `previsionnel`;
CREATE TABLE `previsionnel` (
  `idPrevisionnel` int(11) NOT NULL AUTO_INCREMENT,
  `preparationSol` int(11) NOT NULL,
  `lutteAdventices` int(11) NOT NULL,
  `semis` date NOT NULL,
  `plantation` date NOT NULL,
  `debutRecolte` date NOT NULL,
  `finRecolte` date NOT NULL,
  `idEmplacementReserve` int(11) NOT NULL,
  PRIMARY KEY (`idPrevisionnel`),
  KEY `fk_Previsionnel_EmplacementReserve1_idx` (`idEmplacementReserve`),
  CONSTRAINT `fk_Previsionnel_EmplacementReserve1` FOREIGN KEY (`idEmplacementReserve`) REFERENCES `emplacementreserve` (`idEmplacementReserve`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `semencier`;
CREATE TABLE `semencier` (
  `idSemencier` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(45) NOT NULL,
  PRIMARY KEY (`idSemencier`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO `semencier` VALUES ('1','Semencier1');
INSERT INTO `semencier` VALUES ('2','Semencier2');
INSERT INTO `semencier` VALUES ('3','Semencier3');


DROP TABLE IF EXISTS `suivi`;
CREATE TABLE `suivi` (
  `idSuivi` int(11) NOT NULL AUTO_INCREMENT,
  `preparationSol` date DEFAULT NULL,
  `lutteAdventices` date DEFAULT NULL,
  `semis` date DEFAULT NULL,
  `plantation` date DEFAULT NULL,
  `debutRecolte` date DEFAULT NULL,
  `finRecolte` date DEFAULT NULL,
  `quantite` int(11) DEFAULT NULL,
  `unite` varchar(45) DEFAULT NULL,
  `qualite` int(11) DEFAULT NULL,
  `commentaire` varchar(255) DEFAULT NULL,
  `idPrevisionnel` int(11) DEFAULT NULL,
  `idEmplacementReserve` int(11) NOT NULL,
  PRIMARY KEY (`idSuivi`),
  KEY `fk_Suivi_Previsionnel1_idx` (`idPrevisionnel`),
  KEY `fk_Suivi_EmplacementReserve1_idx` (`idEmplacementReserve`),
  CONSTRAINT `fk_Suivi_Previsionnel1` FOREIGN KEY (`idPrevisionnel`) REFERENCES `previsionnel` (`idPrevisionnel`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_Suivi_EmplacementReserve1` FOREIGN KEY (`idEmplacementReserve`) REFERENCES `emplacementreserve` (`idEmplacementReserve`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


DROP TABLE IF EXISTS `variete`;
CREATE TABLE `variete` (
  `idVariete` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(45) NOT NULL,
  `codeLegume` varchar(45) DEFAULT NULL,
  `commentaire` varchar(255) DEFAULT NULL,
  `idLegume` int(11) NOT NULL,
  `idSemencier` int(11) NOT NULL,
  PRIMARY KEY (`idVariete`),
  KEY `fk_Variete_Legume_idx` (`idLegume`),
  KEY `fk_Variete_Semencier1_idx` (`idSemencier`),
  CONSTRAINT `fk_Variete_Legume` FOREIGN KEY (`idLegume`) REFERENCES `legume` (`idLegume`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_Variete_Semencier1` FOREIGN KEY (`idSemencier`) REFERENCES `semencier` (`idSemencier`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO `variete` VALUES ('1','Coeur de boeuf','TO.CDB','','1','1');
INSERT INTO `variete` VALUES ('2','Cerise','TO.CE','','1','2');
INSERT INTO `variete` VALUES ('3','Roquette','SA.RO','','3','3');


DROP TABLE IF EXISTS `variete`;
CREATE TABLE `variete` (
  `idVariete` int(11) NOT NULL AUTO_INCREMENT,
  `nom` varchar(45) NOT NULL,
  `codeLegume` varchar(45) DEFAULT NULL,
  `commentaire` varchar(255) DEFAULT NULL,
  `idLegume` int(11) NOT NULL,
  `idSemencier` int(11) NOT NULL,
  PRIMARY KEY (`idVariete`),
  KEY `fk_Variete_Legume_idx` (`idLegume`),
  KEY `fk_Variete_Semencier1_idx` (`idSemencier`),
  CONSTRAINT `fk_Variete_Legume` FOREIGN KEY (`idLegume`) REFERENCES `legume` (`idLegume`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_Variete_Semencier1` FOREIGN KEY (`idSemencier`) REFERENCES `semencier` (`idSemencier`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

INSERT INTO `variete` VALUES ('1','Coeur de boeuf','TO.CDB','','1','1');
INSERT INTO `variete` VALUES ('2','Cerise','TO.CE','','1','2');
INSERT INTO `variete` VALUES ('3','Roquette','SA.RO','','3','3');



SET FOREIGN_KEY_CHECKS=1;
