-- MySQL Script generated by MySQL Workbench
-- Tue Feb  3 11:29:34 2015
-- Model: New Model    Version: 1.0
-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema mydb
-- -----------------------------------------------------

-- -----------------------------------------------------
-- Table `Legume`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Legume` (
  `idLegume` INT NOT NULL AUTO_INCREMENT,
  `nom` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idLegume`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Semencier`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Semencier` (
  `idSemencier` INT NOT NULL AUTO_INCREMENT,
  `nom` VARCHAR(45) NOT NULL,
  PRIMARY KEY (`idSemencier`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Variete`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Variete` (
  `idVariete` INT NOT NULL AUTO_INCREMENT,
  `nom` VARCHAR(45) NOT NULL,
  `codeLegume` VARCHAR(45) NULL,
  `commentaire` VARCHAR(255) NULL,
  `idLegume` INT NOT NULL,
  `idSemencier` INT NOT NULL,
  PRIMARY KEY (`idVariete`),
  INDEX `fk_Variete_Legume_idx` (`idLegume` ASC),
  INDEX `fk_Variete_Semencier1_idx` (`idSemencier` ASC),
  CONSTRAINT `fk_Variete_Legume`
    FOREIGN KEY (`idLegume`)
    REFERENCES `Legume` (`idLegume`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Variete_Semencier1`
    FOREIGN KEY (`idSemencier`)
    REFERENCES `Semencier` (`idSemencier`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Emplacement`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Emplacement` (
  `idEmplacement` INT NOT NULL AUTO_INCREMENT,
  `parcelle` VARCHAR(45) NOT NULL,
  `carreCulture` VARCHAR(45) NOT NULL,
  `planche` VARCHAR(45) NOT NULL,
  `codeEmplacement` VARCHAR(45) NULL,
  `commentaire` VARCHAR(255) NULL,
  PRIMARY KEY (`idEmplacement`))
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `EmplacementReserve`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `EmplacementReserve` (
  `idEmplacementReserve` INT NOT NULL AUTO_INCREMENT,
  `idEmplacement` INT NOT NULL,
  `idVariete` INT NOT NULL,
  PRIMARY KEY (`idEmplacementReserve`),
  INDEX `fk_EmplacementReserve_Emplacement1_idx` (`idEmplacement` ASC),
  INDEX `fk_EmplacementReserve_Variete1_idx` (`idVariete` ASC),
  CONSTRAINT `fk_EmplacementReserve_Emplacement1`
    FOREIGN KEY (`idEmplacement`)
    REFERENCES `Emplacement` (`idEmplacement`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_EmplacementReserve_Variete1`
    FOREIGN KEY (`idVariete`)
    REFERENCES `Variete` (`idVariete`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Previsionnel`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Previsionnel` (
  `idPrevisionnel` INT NOT NULL AUTO_INCREMENT,
  `preparationSol` INT NOT NULL,
  `lutteAdventices` INT NOT NULL,
  `semis` DATE NOT NULL,
  `plantation` DATE NOT NULL,
  `debutRecolte` DATE NOT NULL,
  `finRecolte` DATE NOT NULL,
  `idEmplacementReserve` INT NOT NULL,
  PRIMARY KEY (`idPrevisionnel`),
  INDEX `fk_Previsionnel_EmplacementReserve1_idx` (`idEmplacementReserve` ASC),
  CONSTRAINT `fk_Previsionnel_EmplacementReserve1`
    FOREIGN KEY (`idEmplacementReserve`)
    REFERENCES `EmplacementReserve` (`idEmplacementReserve`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB;


-- -----------------------------------------------------
-- Table `Suivi`
-- -----------------------------------------------------
CREATE TABLE IF NOT EXISTS `Suivi` (
  `idSuivi` INT NOT NULL AUTO_INCREMENT,
  `preparationSol` DATE NULL,
  `lutteAdventices` DATE NULL,
  `semis` DATE NULL,
  `plantation` DATE NULL,
  `debutRecolte` DATE NULL,
  `finRecolte` DATE NULL,
  `quantite` INT NULL,
  `unite` VARCHAR(45) NULL,
  `qualite` INT NULL,
  `commentaire` VARCHAR(255) NULL,
  `idPrevisionnel` INT NULL,
  `idEmplacementReserve` INT NOT NULL,
  PRIMARY KEY (`idSuivi`),
  INDEX `fk_Suivi_Previsionnel1_idx` (`idPrevisionnel` ASC),
  INDEX `fk_Suivi_EmplacementReserve1_idx` (`idEmplacementReserve` ASC),
  CONSTRAINT `fk_Suivi_Previsionnel1`
    FOREIGN KEY (`idPrevisionnel`)
    REFERENCES `Previsionnel` (`idPrevisionnel`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_Suivi_EmplacementReserve1`
    FOREIGN KEY (`idEmplacementReserve`)
    REFERENCES `EmplacementReserve` (`idEmplacementReserve`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
